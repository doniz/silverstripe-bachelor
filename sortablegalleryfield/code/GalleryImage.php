<?php 


class GalleryImage extends DataExtension {
 
    private static $db = array(
        'Description' => 'Text',
		'SortOrder' => 'Int'
    );
    
    //FIX
    static $has_one = array( 
        'Page' => 'Page' 
    );
	
	private static $default_sort = "\"Image\".\"SortOrder\" ASC";
	
	public function updateCMSFields(FieldList $fields) {
				
		$fields->addFieldsToTab('Root.Main', 
			array( 
				new TextareaField("Description")
			)
		); 	
	
	}  
    
}

