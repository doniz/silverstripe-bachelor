<?php


class GalleryImageController extends Controller {
	
	//basic security for controller
	static $allowed_actions = array(
		'Sort' => 'ADMIN'
	);

	function init(){
		parent::init();		
	}
    
	/**
     * NEW SORTING METHOD
     */
	function Sort() {
		
        $pageID = $_POST['pageID'];
        $sortIds = explode(',', $_POST['sortIds']);
        //Start transaction if supported
		if(DB::getConn()->supportsTransactions()) {
			DB::getConn()->transactionStart();
		}
        foreach($sortIds as $index=>$id) {
            $id = (int) $id;
            if($id != '') {
                DB::query('UPDATE "Image" SET "SortOrder" = ' . $index
							. ' WHERE "PageID" = ' . $pageID . ' AND "ID" = ' . $id);
            }
        }
        //End transaction if supported
		if(DB::getConn()->supportsTransactions()) {
			DB::getConn()->transactionEnd();
		}
				
	}
	
//	function Sort() {
//		
//		$pageID = $_GET['pageID'];
//		$newPosition  = $_GET['newPosition'];
//		$oldPosition  = $_GET['oldPosition'];
//		
//		//$Images = DataObject::get('Image', '"ParentID" = 8');
//		//$Images = DataList::create('Image')->where('"ParentID" = '.$pageID)->sort('SortOrder ASC');
//        
//        // FIX
//        $Images = DataList::create('Image')->where('"PageID" = '.$pageID)->sort('SortOrder ASC');
// 		
//		$loop = 0;
//		$newSortOrder = 0;
//		foreach($Images as $Image){
//			
//			if ($loop == $newPosition) { $newSortOrder++; }
//			$Image->SortOrder = $newSortOrder;
//			if ($loop == $oldPosition) { $newSortOrder--;  $Image->SortOrder = $newPosition; }
//			
//			$Image->write();
//			
//			$loop++;
//			$newSortOrder++;
//		}
//				
//	}

	
}
