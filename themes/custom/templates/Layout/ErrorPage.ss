<!-- CONTENT -->
<div class="main-content">
    <div class="row">
		<div class="col-md-12">
            <div class="panel error-panel">
                <div class="panel-heading">
                    <h3 class="panel-title"><% if $ErrorCode == 404 %><i class="fa fa-puzzle-piece"></i> <% else %><% if $ErrorCode == 500 %><i class="fa fa-cogs"></i> <% end_if %><% end_if %>$Title</h3>
                </div>
                <div class="panel-body">
                    $Content
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: CONTENT -->