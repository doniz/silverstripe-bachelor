<!DOCTYPE html>
<!--[if !IE]><!-->
<html lang="$ContentLocale">
<!--<![endif]-->
<!--[if IE 6 ]><html lang="$ContentLocale" class="ie ie6"><![endif]-->
<!--[if IE 7 ]><html lang="$ContentLocale" class="ie ie7"><![endif]-->
<!--[if IE 8 ]><html lang="$ContentLocale" class="ie ie8"><![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <% base_tag %>
    <title><% if $MetaTitle %>$MetaTitle<% else %>$Title<% end_if %> &mdash; $SiteConfig.Title</title>
    <link rel="shortcut icon" href="./favicon.png" />

    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css" />-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700&subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
    <link href="$ThemeDir/scripts/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="$ThemeDir/scripts/vendor/bootstrap-jasny/dist/extend/css/jasny-bootstrap.min.css" rel="stylesheet" />
    <!--<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />-->
    <link href="$ThemeDir/scripts/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"  />
    <link href="$ThemeDir/scripts/vendor/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="$ThemeDir/scripts/vendor/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link href="$ThemeDir/scripts/vendor/select2/select2.css" rel="stylesheet" type="text/css" />
    <link href="$ThemeDir/scripts/vendor/select2/select2-bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="$ThemeDir/scripts/vendor/jquery.uniform/themes/default/css/uniform.default.min.css" rel="stylesheet" type="text/css" />
    <link href="$ThemeDir/scripts/css/prettify.css" rel="stylesheet" type="text/css" />
    <link href="$ThemeDir/scripts/vendor/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <link href="$ThemeDir/scripts/vendor/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print" />
    <link href="$ThemeDir/scripts/css/ark.css" rel="stylesheet" type="text/css" />
    <link href="$ThemeDir/scripts/css/common.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>