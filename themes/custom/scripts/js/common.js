(function($) {

	// event actions (profile form editing password)
	$("body").on("click", ".showOnClick a", function(event){
		event.preventDefault();

		var _container = $(this).parent().find(".showOnClickContainer");
		
		if( $(_container).css("display") == 'none' ){
			$(_container).slideDown();
			$(_container).find('input[type="password"]').val('');
		} else {
			$(_container).slideUp();
		}
	});

	if($("#RemoveProfileImage").length){
		$("body").on("click", "#RemoveProfileImage", function(event){
			event.preventDefault();

			window.location.href = $(this).attr("href");
		});
	}

	$('select[name="UsersGroupFilter"]').on("change", function(){
		var _value = $(this).val();

		if(_value != ''){
			window.location.href = _value;
		}
	});


    // var fromDate = $("#TopicTypesForm_TopicTypesForm_ExpireFrom").datepicker({
    //     defaultDate: "+1w",
    //     changeMonth: true,
    //     numberOfMonths: 2,
    //     minDate: new Date(),
    //     onSelect: function(selectedDate) {
    //     	alert(1);
    //         var instance = $(this).data("datepicker");
    //         var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
    //         date.setDate(date.getDate()+30);
    //         toDate.datepicker("option", "minDate", date);
    //     }
    // });
    
    // var toDate = $("#TopicTypesForm_TopicTypesForm_ExpireTo").datepicker({
    //     defaultDate: "+1w",
    //     changeMonth: true,
    //     numberOfMonths: 2
    // });

})(jQuery);