;(function($) {
	$(function(){
		// ContactForm Widget validation
        if($("#ContactsForm_WidgetContactForm").length && $.fn.validate) {
            $("#ContactsForm_WidgetContactForm").validate({
                    errorPlacement: function(error, element) {
                        return false;
                    },
                    highlight: function(element, errorClass, validClass) {
                        var e = $(element).addClass('error');
                        if(!$('#ContactsForm_WidgetContactForm .alert').is(':visible')) {
                            $('#ContactsForm_WidgetContactForm .alert').show();
                        }
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        var e = $(element).removeClass('error');
                        if(!$('#ContactsForm_WidgetContactForm .error').length) {
                            $('#ContactsForm_WidgetContactForm .alert').hide();
                        } 
                    },
                    rules: {
                        Name: "required",
                        Email: {
                            required: true,
                            email: true
                        },
                        Message: "required"
                    },
                    messages: {
                        Name: "",
                        Email: "",
                        Message: ""
                    }//,
//                    submitHandler: function(form) {
//
//                        $('#ContactsForm_WidgetContactForm_doAction').attr('disabled', 'disabled');
//                        var formData = $("#ContactsForm_WidgetContactForm").serializeArray();
//                        formData = JSON.stringify(formData);
//                         $.post( 
//                            urlpath+'/ajaxContactForm/', {'data' : formData}, 
//                            function(data){ 
//                                if(data=='ok') {
//                                    $('#write-comment').hide();
//                                    $('#success-message').show();
//                                    $("input[type=text], input[type=email], textarea").val("");
//                                    $('.contacts-lefter').hide();
//                                    $('#ContactForm_ContactForm_action_doAction').removeAttr('disabled');
//                                }
//                                return false;
//                            }
//
//                        );
//                       return false;
//                    }
            });
        }
	});
}(jQuery));
