;(function($) {
	$(function(){
		// ContactForm validation
       if($("#ContactsForm_ContactsForm").length && $.fn.validate) {  
            $("#ContactsForm_ContactsForm").validate({ 
                    errorPlacement: function(error, element) {
                        return false;
                    },
                    highlight: function(element, errorClass, validClass) {
                        var e = $(element).addClass('error');
                        if(!$('#ContactsForm_ContactsForm .alert').is(':visible')) {
                            $('#ContactsForm_ContactsForm .alert').show();
                        }
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        var e = $(element).removeClass('error');
                        if(!$('#ContactsForm_ContactsForm .error').length) {
                            $('#ContactsForm_ContactsForm .alert').hide();
                        } 
                    },
                    rules: {
                       Name: {
                            required: true,
                            minlength: 2
                        },
                        Email: {
                            required: true,
                            email: true
                        },
                        Message: {
                            required: true,
                            minlength: 5
                        }
                    },
                    messages: {
                        Name: "",
                        Email: "",
                        Message: ""
                    }
            });
        }
	});
}(jQuery));
