<?php
/**
 * @package contacts
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * ContactPage with contact form
 * 
 */
class ContactsPage extends Page {
	
	private static $db = array(
		'SendMessagesTo' => 'Varchar',
		'ShowContentAtFooter' => 'Boolean(true)'
	);
	
	// One to many relationship with Contact object
	private static $has_many = array(
		'UserFeedbacks' => 'UserFeedback'
	);
    
    private static $description = "Contact Page with contact form and google maps";
    
    private static $icon = "mysite/treeicons/contact-icon.png";

	public function getCMSFields() {
		// Get the fields from the parent implementation
		$fields = parent::getCMSFields();
		
		$feedbacks = GridFieldConfig_RelationEditor::create();
		// Set the names and data for our gridfield columns
		$feedbacks->getComponentByType('GridFieldDataColumns')->setDisplayFields(array(
			'Name' => _t('ContactsPage.Name', 'Name'),
			'Email' => _t('ContactsPage.Email', 'Email'),
            'Created' => _t('ContactsPage.Date', 'Date')
		));
        
		// $feedbacks->addComponent(new GridFieldAjaxRefresh(0, false)); NO AJAX REFRESH Library fix  
		$gridField = new GridField(
				'UserFeedbacks', // Field name
				'UserFeedback', // Field title
				$this->UserFeedbacks(), 
				$feedbacks
		);
		// Create a tab named "Students" and add our field to it
		$fields->addFieldToTab('Root.Feedbacks', $gridField);
		
		$fields->addFieldToTab('Root.Main', 
            TextField::create('SendMessagesTo', 
                _t('ContactsPage.SendMessageTo', 'Send Message to')), 
            'Content'
        );

        $fields->addFieldToTab('Root.Main',
        	CheckboxField::create('ShowContentAtFooter',
        		_t('ContactsPage.ShowContentAtFooter', 'Show Content At Footer')),
        	'Content'
        );
		
		return $fields;
	}
	
	
}

class ContactsPage_Controller extends Page_Controller {
	
	public static $allowed_actions = array(
		'ContactsForm'
	);

	public function init(){
		parent::init();

		// <-- Start Google Maps variables
		$GMapMarker = (substr($this->GMapMarker()->Link(), -strlen(".png")) === ".png") ? $this->GMapMarker()->Link() : '';

		$GoogleConfig = SiteConfig::current_site_config();
		$customVariables = array(
			"_gmap_lat" => (!empty($this->GMapLat)) ? $this->GMapLat : 0,
			"_gmap_lng" => (!empty($this->GMapLng)) ? $this->GMapLng : 0,
			"_gmap_zoom" => (!empty($this->GMapZoom)) ? $this->GMapZoom : 0,
			// "_gmap_heading" => (!empty($this->GMapHeading)) ? $this->GMapHeading : 0,
			// "_gmap_pitch" => (!empty($this->GMapPitch)) ? $this->GMapPitch : 0,
			"_gmap_marker" => (!empty($GMapMarker)) ? Director::AbsoluteURL($GMapMarker) : '',
			"_gmap_element" => "GMap_Map",
			"_gmap_api" => (!empty($GoogleConfig->GMapAPI)) ? $GoogleConfig->GMapAPI : '',
			"_gmap_balloon" => (!empty($this->GMapBalloon)) ? $this->GMapBalloon : ''
		);

		Requirements::javascriptTemplate(GMAPMODULE_PATH . '/javascript/GMap.min.js', $customVariables);
		// EOF Google Maps variable -->
	}
	
	public function ContactsForm() {
        $f = new ContactsForm($this, 'ContactsForm');
        $f->clearMessage();
        return $f;        
	}

}