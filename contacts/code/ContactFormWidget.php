<?php
/**
 * @package contacts
 * @author Neringa Rigertaite <neringa@idea.lt>
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * Contacts Widget - show contact form
 * 
 */
class ContactFormWidget extends Widget {

	private static $title = 'Contact Form Widget';
	private static $cmsTitle = 'Contact Form Widget';
	private static $description = 'show contact';
    
	private static $db = array(
        'WidgetTitle' => 'Varchar(255)',
        'WidgetText' => 'Text'
	);
    
    public function getCMSFields() {

		return new FieldList(
			new TextField("WidgetTitle", _t('ContactWidget.Title', 'Title')),
            new TextareaField("WidgetText", _t('ContactWidget.WidgetText', 'Text'))
		);
	}

   
}

class ContactFormWidget_Controller extends Widget_Controller {

    private static $allowed_actions = array (
		'WidgetContactForm', 'doAction'
	);
    
    public function init() {
        parent::init();
        Requirements::javascript(CONCTACTS_DIR . '/javascript/contactformwidget.js');
    }
    
    public function doAction(array $data, Form $form) {
        
        $currentPage = Director::get_current_page();
        
        $contactItem = new UserFeedback();
		$contactItem->ContactsPageID = $currentPage->ID;
		$form->saveInto($contactItem);
		$contactItem->write();
		Session::set('UserFeedbackSubmitted', true);

        $contactItem->PageURL = $currentPage->AbsoluteLink();

        if(!$toEmail) {
            $config = SiteConfig::current_site_config(); 
            $toEmail = ($config && !empty($config->DefaultAdminEmail)) ? $config->DefaultAdminEmail : false;
        }
        
		$email = new Email(
			$from = _t('ContactsForm.EmailFrom', 'www-noreply@doniz.net'), 
			$toEmail, 
			$subject = _t('ContactsForm.EmailSubject', 'Message form Contacts Form')
		);
        $email->setTemplate('ContactEmail');
        $email->populateTemplate($contactItem); 
        $email->send();
		//$email->sendPlain();
		
		return Controller::curr()->redirectBack();
    }

    public function WidgetContactForm() {
        $f = new ContactsForm($this, 'WidgetContactForm');
        $f->clearMessage();
        return $f;        
	}

}