<?php
/**
 * @package contacts
 * @author Neringa Rigertaite <neringa@idea.lt>
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * Admin for contacts module - all user feedbacks
 * 
 */
class UserFeedbackAdminAdmin extends ModelAdmin {
        private static $managed_models = array(
                'UserFeedback'
        );

        private static $url_segment = 'feedbacks';

        private static $menu_title = 'Feedbacks';
        
        public $showImportForm = false;

        public function getEditForm($id = null, $fields = null) {
            $form = parent::getEditForm($id, $fields);
            $siteConfig = SiteConfig::current_site_config();
           // $gridField = $form->Fields()->fieldByName($this->sanitiseClassName($this->modelClass));
            //$gridField->getConfig()->addComponent(new GridFieldSortableRows('Created'));
            
            if($this->modelClass == "UserFeedback" && !$siteConfig->AllowExport){
                    $form->Fields()
                            ->fieldByName("UserFeedback")
                            ->getConfig()
                            ->removeComponentsByType('GridFieldExportButton');
            }
            

            return $form;
        }

}