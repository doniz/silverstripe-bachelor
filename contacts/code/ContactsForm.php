<?php
/**
 * @package contacts
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * Contact Form
 * 
 */
class ContactsForm extends Form {

	public function __construct($controller, $name) {
		$fields = new FieldList(
			TextField::create('Name')
				->addExtraClass('input'),
			EmailField::create("Email")
				->addExtraClass('input'),
			TextareaField::create("Message")
				->addExtraClass('el_372')
		);
		$actions = new FieldList(
			FormAction::create("doAction")
				->setTitle(_t('ContactsForm.Send', "Send"))
				->addExtraClass('submit')
		);
		
		$validator = new RequiredFields('Name', 'Email', 'Message');
		
		parent::__construct($controller, $name, $fields, $actions, $validator);
	}
	
	public function Success() {
		$submitted = Session::get('UserFeedbackSubmitted');
		Session::clear('UserFeedbackSubmitted');
		return !empty($submitted);
	}
	
	public function doAction(array $data, Form $form) {
		$contactItem = new UserFeedback();
		$contactItem->ContactsPageID = Director::get_current_page()->ID;
		$form->saveInto($contactItem);
		$contactItem->write();
		Session::set('UserFeedbackSubmitted', true);
        
        $controller = Controller::curr();
        $contactItem->PageURL = $controller->AbsoluteLink();

        $toEmail = (isset(Director::get_current_page()->SendMessagesTo)) ? Director::get_current_page()->SendMessagesTo : false;
        if(!$toEmail) {
            $config = SiteConfig::current_site_config(); 
            $toEmail = ($config && !empty($config->DefaultAdminEmail)) ? $config->DefaultAdminEmail : false;
        }
        
		$email = new Email(
			$from = _t('ContactsForm.EmailFrom', 'www-noreply@doniz.net'), 
			$toEmail, 
			$subject = _t('ContactsForm.EmailSubject', 'Message form Contacts Form')
		);
        $email->setTemplate('ContactEmail');
        $email->populateTemplate($contactItem); 
        $email->send();
		//$email->sendPlain();
		
		return Controller::curr()->redirectBack();
	}

	public function forTemplate() {
		return $this->renderWith(array($this->class, 'Form'));
	}

}