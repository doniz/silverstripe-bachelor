<?php
/**
 * @package contacts
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * Items from ContactForm
 * 
 */
class UserFeedback extends DataObject {

	private static $db = array(
		'Name' => 'Varchar',
		'Email' => 'Text',
		'Message' => 'Text',
	);
	private static $has_one = array(
		'ContactsPage' => 'Page',
	);
    
    private static $default_sort = 'Created DESC';
    
	private static $summary_fields = array(
		'Name' => 'Name',
		'Email' => 'Email',
		'Message' => 'Message',
        'Created' => 'Date',
        'ContactsPageTitle' => 'Page'
	);
    
    
	public function getCMSFields() {

		return new FieldList(
			new TextField('Name', _t('UserFeedback.Name', 'Name')),
			new EmailField('Email', _t('UserFeedback.Email', 'Email')),
			new TextareaField('Message', _t('UserFeedback.Message', 'Message'))
		);
	}
    
    public function ContactsPageTitle() {
       
        $page = SiteTree::get_by_id('SiteTree', $this->ContactsPageID);
        return ($page) ? $page->Title : false;
    }

}