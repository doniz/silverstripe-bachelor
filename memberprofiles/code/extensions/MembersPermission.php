<?php

class MembersPermission extends DataExtension {
     
    static $db = array(
    );
    
    static $defaults = array(

    );

    static $has_one = array(
    	"SYSADMIN_Group" => "Group",
    	"MANAGER_Group"  => "Group",
    	"STUDENT_Group"  => "Group",
    	"LECTURER_Group" => "Group",
    	"ADMIN_Group"    => "Group"
    );
 
    public function updateCMSFields(FieldList $fields){
        $fields->addFieldToTab('Root', Tab::create('MembersPermissions', _t('MembersPermissions.TabMembersPermissions', 'Vartotojų teisių grupės')));
        $fields->addFieldsToTab('Root.MembersPermissions', array(
            $SYSADMIN_Group = TreeDropdownField::create(
            	"SYSADMIN_GroupID",
            	_t("MembersPermission.SYSADMIN_Group", "Sistemos administratoriaus grupė"),
            	"Group"
            ),
            $MANAGER_Group = TreeDropdownField::create(
            	"MANAGER_GroupID",
            	_t("MembersPermission.MANAGER_Group", "Vedėjo grupė"),
            	"Group"
            ),
            $STUDENT_Group = TreeDropdownField::create(
            	"STUDENT_GroupID",
            	_t("MembersPermission.STUDENT_Group", "Studento grupė"),
            	"Group"
            ),
            $LECTURER_Group = TreeDropdownField::create(
            	"LECTURER_GroupID",
            	_t("MembersPermission.LECTURER_Group", "Dėstytojo grupė"),
            	"Group"
            ),
            $ADMIN_Group = TreeDropdownField::create(
            	"ADMIN_GroupID",
            	_t("MembersPermission.ADMIN_Group", "Administratoriaus grupė"),
            	"Group"
            )
        ));
    }

}