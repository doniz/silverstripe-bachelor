<?php

class MembersPage extends Page {

	private static $db = array(
		"ItemsPerPage" => "Int"
	);

	private static $has_one = array(
	);

	private static $defaults = array(
		"CanViewType"  => "OnlyTheseUsers",

		// Default permissions
		"ViewerGroups" => array(1,2,3,4),
			// 1 -> "Managers"
			// 2 -> "Administrators"
			// 3 -> "Students"
			// 4 -> "Lectures"
		"CanEditType" => "OnlyTheseUsers",
		"EditorGroups" => array(2),
			// 2 -> "Administrators"

		"ShowInSearch" => false,
		"ShowInFooterMenu" => false,
		"ShowInMenus" => true,
		"ItemsPerPage" => 10 // 10 items per page (default)
	);

	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->addFieldToTab(
			"Root.Main",
			TextField::create(
				"ItemsPerPage",
				_t("MembersPage.ItemsPerPage", "Vartotojų įrašų per puslapį (įveskite skaičių)")
			),
			"Content"
		);

		return $fields;
	}

	public function getUsersGroups(){
		return singleton("Group")->get()->filter(array("ParentID" => 0));
	}

	public function getUsersGroupById($id = null){
		$Group = singleton("Group")->get()->filter(array("ID" => $id));

	}

	public function getGroupsByUserId($id = 0){
		$Member = singleton("Member")->get()->filter(array("ID" => $id))->First();

		$UsersGroups = array();
		foreach($Member->Groups() as $group){
			array_push($UsersGroups, $this->TranslateGroupById($group->ID));
		}

		return implode(", ", $UsersGroups);
	}

	public function getUsersByGroupId($id = null){
		if(!is_null($id)){
			$temp_users = new ArrayList();

			$Users = singleton("Group")->get()->filter(array("ID" => $id))->First()->DirectMembers()->sort("FirstName", "ASC");

			foreach($Users as $User){
				$User->NameURLSegment = SiteTree::generateURLSegment(
					sprintf("%s-%s", $User->Name, $User->ID)
				);

				$temp_users->push($User);
			}

			return $temp_users;
		}

		return false;
	}

	public function getUsersByGroupTitle($title = null){
		if(!is_null($title)){
			$temp_users = new ArrayList();

			$Users = singleton("Group")->get()->filter(array("Title" => $title))->First()->DirectMembers()->sort("FirstName", "ASC");

			foreach($Users as $User){
				$User->NameURLSegment = SiteTree::generateURLSegment(
					sprintf("%s-%s", $User->Name, $User->ID)
				);

				$temp_users->push($User);
			}

			return $temp_users;
		}

		return false;
	}

	public function getUserByNameURLSegment($URLSegment){
		// get all groups
		$groups = $this->getUsersGroups();
		$temp_users = new ArrayList();

		foreach($groups as $group){
			$users = singleton("Group")->get()->filter(array("ID" => $group->ID))->First()->DirectMembers()->sort("FirstName", "ASC");

			foreach($users as $user){
				$NameURLSegment = SiteTree::generateURLSegment(
					sprintf("%s-%s", $user->Name, $user->ID)
				);

				if($NameURLSegment == $URLSegment){
					return $user;
				}
			}
		}

		return false;
	}

	private static function ObjectToArray($object){

		if(is_object($object) && $object instanceof stdClass ){

			$temp_array = array();

			foreach($object as $key => $value){

				$temp_array[$key] = $value;
			}

			return $temp_array;

		}

	}

	public function getUsersByFilters($perPage = 1){

		$Users = (isset($_REQUEST["UsersGroup"])) ? $this->getUsersByGroupId($_REQUEST["UsersGroup"]) : $this->getUsersByGroupId($this->GetStudentsGroupId());

		set_include_path(
			get_include_path() . PATH_SEPARATOR . BASE_PATH . '/thirdparty/'
		);

		require_once BASE_PATH . "/thirdparty/Zend/Paginator.php";
		require_once BASE_PATH . "/thirdparty/Zend/Paginator/Adapter/Array.php";

		$temp_users = array();
		foreach($Users as $User){
			$User->_Groups = $this->getGroupsByUserId($User->ID);
			array_push($temp_users, $User);
		}

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($temp_users));
        $paginator->setCurrentPageNumber(
        	(isset($_REQUEST["page"])) ? (int) $_REQUEST["page"] : 1
        );


        // if is set
		if ( !empty( $this->ItemsPerPage ) ) {
			
			$perPage = $this->ItemsPerPage;
		
		}

        $paginator->setItemCountPerPage($perPage);

        $data = new ArrayList(
        	iterator_to_array($paginator->getCurrentItems(), false)
        	
        );

		$gettedPages = $paginator->getPages();


		if(is_array($gettedPages->pagesInRange)){
			$temp_pagesInRange = array();
			$pagesInRange = $gettedPages->pagesInRange;
			
			foreach($pagesInRange as $key => $value){
				array_push($temp_pagesInRange, array("item" => $value));
			}

			$gettedPages->pagesInRange = new ArrayList($temp_pagesInRange);
		}

		$data->pages = new ArrayData($gettedPages);

		$data->UsersGroupRequest = (isset($_REQUEST["UsersGroup"])) ? $_REQUEST["UsersGroup"] : false;

		$data->currentPage = (isset($_REQUEST["page"])) ? (int) $_REQUEST["page"] : 1;

		return $data;


	}

	public function getCurrentFilteredGroupId(){

		if(isset($_REQUEST["UsersGroup"])){
			return $_REQUEST["UsersGroup"];
		}

		return $this->GetStudentsGroupId();
	}

	public function getCurrentFilteredGroupTitle(){

		if(isset($_REQUEST["UsersGroup"])){
			return $this->TranslateGroupById($_REQUEST["UsersGroup"]);
		}

		return $this->TranslateGroupById($this->GetStudentsGroupId());
	}

}

class MembersPage_Controller extends Page_Controller {

	private static $allowed_actions = array(
		"user"
	);

	public function init(){
		parent::init();
	}

	public function user($request){

		$user = $this->getUserByNameURLSegment($request->param('ID'));

		if( $user instanceof Member && is_int($user->ID) && $user->ID > 0){

			$CurrentUsersGroups = array();
			foreach($user->Groups() as $group){
				array_push($CurrentUsersGroups, $this->TranslateGroupById($group->ID));
			}

			$data = array(
				"Title" => $user->Name,
				"Content" => "",
				"DescriptionAboutPage" => "",
				"CurrentRequestUser" => $user,
				"CurrentRequestUserGroupName" => implode(", ", $CurrentUsersGroups)
			);

			$this->Children()->push($data);

			return $this->customise($data)->renderWith(array('MembersPage_user', 'MembersPage', 'Page'));
		}


		return $this->redirectBack();

	}
}
