<?php

class AccessForm extends Form {

	public function __construct($controller, $name) {
		$fields = new FieldList(
			TextField::create('AccessCode')
		);
		$actions = new FieldList(
			FormAction::create("SubmitAccessCode")
				->setTitle(_t('AccessCode.Send', "Send"))
		);
		
		$validator = new RequiredFields('AccessCode');
		
		parent::__construct($controller, $name, $fields, $actions, $validator);
	}

	public function Success() {
		$submitted = Session::get('UserFeedbackSubmitted');
		Session::clear('UserFeedbackSubmitted');
		return !empty($submitted);
	}
	
	public function SubmitAccessCode(array $data, Form $form) {

		if(!empty($data["AccessCode"])){

			$object = singleton("MemberRegistrationSettings")->get("MemberRegistrationSettings", "AccessCode = '" . $data["AccessCode"] . "' AND ExpireFrom <= CURDATE() AND ExpireTo >= CURDATE()");

			if($object->Count() > 0){
				$object = $object->First();

				Session::set("SSAccessCode", $data["AccessCode"]);
			}
		}

		return Controller::curr()->redirectBack();
	}

	public function forTemplate() {
		return $this->renderWith(array($this->class, 'Form'));
	}

}