<?php

class MemberRegistrationSettings extends DataObject {

	private static $db = array(
		"AccessCode" => "Varchar(20)",
		"ExpireFrom" => "Date",
		"ExpireTo"   => "Date"
	);

	private static $has_one = array(
		'MemberProfilePage' => 'MemberProfilePage'
	);

	private static $many_many = array (
		'RegistrationGroup' => 'Group'
	);

	public function RegistrationAllowed(){
		$groups = array();

		foreach($this->RegistrationGroup() as $Group){
			array_push($groups, $Group->Title);
		}

		return implode(", ", $groups);
	}

	public function getCMSFields(){
		return new FieldList(
			$AccessCode = TextField::create(
				"AccessCode",
				_t("MemberRegistrationSettings.ACCESSCODE", "Prieigos kodas")
			),
			$ExpireFrom = DateField::create(
				"ExpireFrom",
				_t("MemberRegistrationSettings.EXPIREFROM", "Galioja nuo")
			)->setConfig("showcalendar", true),
			$ExpireTo = DateField::create(
				"ExpireTo",
				_t("MemberRegistrationSettings.EXPIRETO", "Galioja iki")
			)->setConfig("showcalendar", true),
			$RegistrationGroup = TreeMultiselectField::create(
				'RegistrationGroup',
				_t('MemberRegistrationSettings.REGISTRATIONGROUP', 'Registracijos grupė'),
				'Group'
			)
		);

        $ExpireFrom->setConfig('dateformat', 'yyyy-MM-dd'); //->setConfig('max', date("Y-m-d", time()));
        $ExpireTo->setConfig('dateformat', 'yyyy-MM-dd'); //->setConfig('max', date("Y-m-d", time()));

	}

}