<?php
/**
 * 
 * @author Donatas Navidonskis (donatas@idea.lt)
 * @license GPL
 * @link www.doniz.net
 * @since 2013
 * @version 1.0
 *
 * @package Google Maps module
 *
 * Google maps settings to set up API's, etc.
*/
class GMapSettings extends DataExtension {

	/**
	 * Variables of database 
	 * @var array
	 * @static $db
	 *
	*/
	private static $db = array(
		 "GMapAPI"   => "Varchar(255)"
		// #Tip: uncomment those lines if StreetView enabled or want to use that feature
		// ,"GMapSView" => "Boolean" 
	);

	/**
	 * Updating main cms fields
	 * @param FieldList $fields
	 *
	*/
	public function updateCMSFields(FieldList $fields){
		$apiKey = TextField::create(
			"GMapAPI",
			_t("{$this->class}.GMAP_API_KEY", "Google API Key (optional)")
		);

		// #Tip: uncomment those lines if StreetView enabled or want to use that feature
		// $streetView = DropdownField::create(
		// 	"GMapSView",
		// 	_t("{$this->class}.SHOW_GMAP_STREET_VIEW", "Rodyti Google gatvės vaizdą ?"),
		// 	array(
		// 		0 => _t("{$this->class}.NO", "Ne"),
		// 		1 => _t("{$this->class}.YES", "Taip")
		// 	)
		// );

		$fields->addFieldToTab(
			"Root",
			Tab::create(
				"Google Maps",
				_t("{$this->class}.GOOGLEMAPS_TITLE", "Google Map"),
				$apiKey
				// #Tip: uncomment those lines if StreetView enabled or want to use that feature
				// ,$streetView
			)
		);
	}

}