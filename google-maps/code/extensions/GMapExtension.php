<?php
/**
 * 
 * @author Donatas Navidonskis (donatas@idea.lt)
 * @license GPL
 * @link www.doniz.net
 * @since 2013
 * @version 1.0
 *
 * @package Google Maps module
 *
 * Google maps extension for defined page via Object::add_extension.
 * Additional database/form to hold map records
*/
class GMapExtension extends DataExtension {

	/**
	 * Variables of database 
	 * @var array
	 * @static $db
	 *
	*/
	private static $db = array(
		 "GMapLat"     => "Varchar"
		,"GMapLng"     => "Varchar"
		,"GMapZoom"    => "Int"
		,"GMapBalloon" => "HTMLText"
		// #Tip: uncomment those lines if StreetView enabled or want to use that feature
		// ,"GMapHeading" => "Varchar" // using if street view is enabled
		// ,"GMapPitch"   => "Varchar" // using if street view is enabled
	);

	/**
	 * default defined variable values
	 * @var array
	 * @static $defaults
	 *
	*/
	private static $defaults = array(
		 "GMapZoom" => 15,
		 "GMapLat"  => 54.695662,
		 "GMapLng"  => 25.280807
	);

	/**
	 * variables of objects
	 * @var array
	 * @static $has_one
	 *
	*/
	private static $has_one = array(
		 "GMapMarker" => "BetterImage"
	);

	/**
	 * Updating main cms fields
	 * @param FieldList $fields
	 *
	*/
	public function updateCMSFields(FieldList $fields){
		// get config settings
		$SiteConfig = SiteConfig::current_site_config();

		// Javascript to determined with map & fields
		Requirements::javascript(GMAPMODULE_PATH . "/javascript/GMapSSPlugin.js");

		$GMapMarker = UploadField::create(
			"GMapMarker",
			_t("{$this->class}.GMAP_MARKER_IMAGE", "Marker Image (.png)")
		);
		$GMapMarker->setFolderName("Uploads/");
		$GMapMarker->getValidator()->setAllowedExtensions(array("png"));

		$GMapLat  = HiddenField::create("GMapLat");
		$GMapLng  = HiddenField::create("GMapLng");
		$GMapZoom = HiddenField::create("GMapZoom");

		// #Tip: uncomment those lines if StreetView enabled or want to use that feature
		// $GMapHeading = HiddenField::create("GMapHeading");
		// $GMapPitch = HiddenField::create("GMapPitch");

		
		// generate data to rendering at template for javascript
		$data = array(
			 "SS_Google_API"     => (!empty($SiteConfig->GMapAPI)) ? $SiteConfig->GMapAPI : ''
			// #Tip: uncomment those lines if StreetView enabled or want to use that feature
			// ,"SS_Google_STREET"  => (!empty($SiteConfig->GMapSView)) ? $SiteConfig->GMapSView : 0
		);
		// map template
		$MapField = LiteralField::create(
			"GMapTemplate",
			$this->owner->renderWith("GMap_Fields", $data)
		);

		$GMapBalloon_Label = LiteralField::create(
			"GMapBalloon_Label",
			"<div class=\"field text\"><strong>" . _t("{$this->class}.GMAPBALLOON_LABEL", "Google Map Balloon") . "</strong></div>"
		);
		$GMapBalloon = HTMLEditorField::create(
			"GMapBalloon",
			"" // leave empty
		);

		$GMapBalloon->setRows(15);

		$fields->addFieldToTab(
			"Root",
			Tab::create(
				"Google Maps",
				_t("{$this->class}.GOOGLEMAPS_TITLE", "Google Map"),
				$GMapLat      // Latitude Field
				,$GMapLng     // Longitude Field
				,$GMapZoom    // Zoom Field
				// #Tip: uncomment those lines if StreetView enabled or want to use that feature
				// ,$GMapHeading  // Heading Field
				// ,$GMapPitch    // Pitch Field
				,$GMapMarker   // Google marker field (image)
				,$MapField      // Custom template field
				,$GMapBalloon_Label // Label information for HTML field
				,$GMapBalloon // Info window field
			)
		);

		return $fields;
	}
}