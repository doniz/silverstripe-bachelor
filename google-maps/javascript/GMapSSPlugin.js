/**
 * 
 * @author Loz Calver - Bigfork Ltd.
 * @edited Donatas Navidonskis (donatas@idea.lt)
 * @package Google Maps module
 *
 * JS Plugin for Google Maps
*/
var ss = ss || {};

// has Loaded API ?
ss.hasLoadedGMapAPI = false;

// main functions
(function($) {
    $.entwine('ss', function($){
        /**
         * initialize map upon matching appropriate field
         *
        */
        $("#GMap_Map").entwine({
            // constructor
            onmatch: function(){
                if( !ss.hasLoadedGMapAPI ){
                    var script = document.createElement("script"), key = $("#GMap_API").html();
                    key = (key != '') ? '?key=' + key + '&': '?';
                    script.type = "text/javascript";
                    script.src = "http://maps.googleapis.com/maps/api/js" + key + "sensor=false&callback=initGoogleMaps";
                    document.body.appendChild(script);
                    ss.hasLoadedGMapAPI = true;
                } else {
                    initGoogleMaps();
                }
            }
        });
    });
}(jQuery));

/**
 * initializing Google Maps
 * @param none
 * @return none
 *
*/
var initGoogleMaps = function(){
    (function($){
        /**
         * Wrapper for Google Maps objects
         */
        ss.GMapsObject = {
            latField: $('.cms-edit-form input[name=GMapLat]'),
            lngField: $('.cms-edit-form input[name=GMapLng]'),
            headingField: $('.cms-edit-form input[name=GMapHeading]'),
            pitchField: $('.cms-edit-form input[name=GMapPitch]'),
            zoomField: $('.cms-edit-form input[name=GMapZoom]'),
            balloonField: $('#Form_EditForm_GMapBalloon'),
            markerField: $('.cms-edit-form #GMapMarker img').attr("src"),
            streetViewEnabled: ($('#GMap_StreetView').length !== 0)
        };

        // validation for GMapMarker image
        ss.GMapsObject.markerField = (ss.GMapsObject.markerField != undefined && ss.GMapsObject.markerField != '') ? addURL(ss.GMapsObject.markerField) : null;

        var mapCenter = new google.maps.LatLng(
                (ss.GMapsObject.latField.val()) ? parseFloat(ss.GMapsObject.latField.val()) : 54.695662,
                (ss.GMapsObject.lngField.val()) ? parseFloat(ss.GMapsObject.lngField.val()) : 25.280807
            ),
            mapOptions = {
                zoom: (ss.GMapsObject.zoomField.val() && ss.GMapsObject.zoomField.val() > 0) ? parseInt(ss.GMapsObject.zoomField.val()) : 15,
                center: mapCenter,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            },
            map = new google.maps.Map(document.getElementById('GMap_Map'), mapOptions);

        if (ss.GMapsObject.streetViewEnabled) {
            var panoramaOptions = {
                    position: mapCenter,
                    visible: true,
                    pov: {
                        heading: (ss.GMapsObject.headingField.val()) ? parseFloat(ss.GMapsObject.headingField.val()) : 0,
                        pitch: (ss.GMapsObject.pitchField.val()) ? parseFloat(ss.GMapsObject.pitchField.val()) : 0
                    }
                },
                panorama = new google.maps.StreetViewPanorama(document.getElementById("GMap_StreetView"), panoramaOptions);

            mapOptions.streetViewControl = true;
            mapOptions.streetView = panorama;
            map.setOptions(mapOptions);
            
            google.maps.event.addListener(panorama, "position_changed", function() {
                map.setCenter(panorama.getPosition());
                updateLatLng();
            });
            google.maps.event.addListener(panorama, "pov_changed", updatePOV);
            ss.GMapsObject.marker = panorama;
            ss.GMapsObject.panorama = panorama;

        } else {
            if(ss.GMapsObject.markerField != null){
                var marker = new google.maps.Marker({
                    position: mapCenter,
                    map: map,
                    draggable: true,
                    icon: ss.GMapsObject.markerField
                });
            } else {
                var marker = new google.maps.Marker({
                    position: mapCenter,
                    map: map,
                    draggable: true
                });
            }

            /**
             * Info window on load
             *
            */
            if(ss.GMapsObject.balloonField.val() != ''){
                google.maps.event.addListener(marker, 'click', function(){
                    infowindow = new google.maps.InfoWindow({
                        content: ss.GMapsObject.balloonField.val()
                    });
                    infowindow.open(map, marker);
                });
            }

            google.maps.event.addListener(marker, 'dragend', updateLatLng);
            ss.GMapsObject.marker = marker;
        }

        /**
         * if zoom is changed, then add to field new values
         *
        */
        google.maps.event.addListener(map, 'zoom_changed', function() {
          ss.GMapsObject.zoomField.val(map.getZoom()).change();
        });

        ss.GMapsObject.map = map;
        ss.GMapsObject.geocoder = new google.maps.Geocoder();

        $.entwine('ss', function($) {

            /**
             * ID: #Root_GoogleMap[aria-hidden="false"]
             *
             * Redraw map when tab becomes visible. Trigger resize event when switching tabs as
             * map will render at 0 width/height in inactive tabs. Also need to re-center after
             */
            $('#Root_GoogleMaps[aria-hidden="false"]').entwine({
                // Constructor: onmatch
                onmatch: function() {
                    var map = ss.GMapsObject.map;
                    google.maps.event.trigger(map, 'resize');

                    if (ss.GMapsObject.streetViewEnabled) {
                        var panorama = ss.GMapsObject.panorama;
                        google.maps.event.trigger(panorama, 'resize');
                    }

                    center = ss.GMapsObject.marker.getPosition();
                    map.panTo(center);
                }
            });

            /**
             * Class: .cms-edit-form input[name=SearchAddress]
             *
             * Bind events for geocoding address
             */
            $('.cms-edit-form input[name=SearchAddress]').entwine({
                // Constructor: onmatch
                onmatch: function() {
                    var self = this;

                    this.bind('keypress blur', function(e) {
                        if (d) clearTimeout(d);
                        // Set timeout to prevent lots of maps API requests
                        var d = setTimeout(function() {
                            geocodePosition(self.val());
                        }, 500);
                    });
                }
            });

            /**
             * Id: #Form_EditForm_GMapBalloon
             *
             * Bind events to create info window
             */
            $('#Form_EditForm_GMapBalloon').entwine({
                onmatch: function(){
                    var self = this;

                    this.bind('change', function(e){
                        google.maps.event.addListener(marker, 'click', function(){
                            infowindow = new google.maps.InfoWindow({
                                content: self.val()
                            });
                            infowindow.open(map, marker);
                        });
                    });
                }
            });
        });


    }(jQuery));


};

/**
 * Updates the hidden fields for coordinates and triggers an onchange event as
 * 3.1's changetracker needs that event to pick up the changes
 */
function updateLatLng() {
    var latLng = ss.GMapsObject.marker.getPosition();

    // Check if the value has actually changed - as panorama triggers this onload
    if (latLng.lat() !== parseFloat(ss.GMapsObject.latField.val()) &&
        latLng.lng() !== parseFloat(ss.GMapsObject.lngField.val())
    ) {
        ss.GMapsObject.latField.val(latLng.lat()).change();
        ss.GMapsObject.lngField.val(latLng.lng()).change();
    }
}

/**
 * Update the heading and pitch values when moving around in street view
 */
var povTimeout;
function updatePOV() {
    if (povTimeout) clearTimeout(povTimeout);

    povTimeout = setTimeout(function() {
       var pov = ss.GMapsObject.marker.getPov();
        ss.GMapsObject.headingField.val(pov.heading).change();
        ss.GMapsObject.pitchField.val(pov.pitch).change();
    }, 100);
}

/**
 * Converts an address/location into a LatLng object and updates map & hidden
 * fields with coordinates
 */
function geocodePosition(address) {
    ss.GMapsObject.geocoder.geocode({
            'address': address
        }, function(responses) {
            if (responses && responses.length > 0) {
                var lat = responses[0].geometry.location.lat(),
                    lng = responses[0].geometry.location.lng(),
                    center = new google.maps.LatLng(lat, lng);

                ss.GMapsObject.map.setCenter(center);
                ss.GMapsObject.marker.setPosition(center);
                updateLatLng();
            }
        }
    );
}

/**
 * function adding prefix of hostname and protocol to
 * generate with given param name _given_string
 * @param string _given_string
 * @return string (uri)
 *
*/
var addURL = function(_given_string){
    if(typeof _given_string != undefined && _given_string != ''){
        var protocol = window.location.protocol;
        var hostname = window.location.host;

        if(protocol.indexOf(":")){
            protocol = protocol.substring(0, protocol.indexOf(":"));
        }

        return protocol + "://" + hostname + _given_string;
    }
};