/**
 * 
 * @author Donatas Navidonskis (donatas@idea.lt)
 * @license GPL
 * @link www.doniz.net
 * @since 2013
 * @version 1.0
 *
 * @package Google Maps module
 *
 * Google Maps for front-end implementation
*/

/**
 * validating if exist variable and not empty
 * @param object variable
 * @return boolean
 *
*/
var validationOf = function(variable){
    if(typeof variable != undefined
        && variable != ''){
        return true;
    }
    return false;
};

/**
 * Google Maps options. Variables defined by 
 * SS3 beckend init function
 * @var object
 *
*/
var _google_objects = {
    _gmap_lat : $_gmap_lat,
    _gmap_lng : $_gmap_lng,
    _gmap_zoom : $_gmap_zoom,
    _gmap_element : '$_gmap_element',
    _gmap_marker : '$_gmap_marker',
    _gmap_api : '$_gmap_api',
    _gmap_balloon: '$_gmap_balloon'
    // _gmap_pitch : $_gmap_pitch,
    // _gmap_heading : $_gmap_heading
};

/**
 * initializing Google Maps
 * @param none
 * @return none
 *
*/
var initialize = function(){
    if(validationOf(_google_objects._gmap_lat) 
        && validationOf(_google_objects._gmap_lng) 
            && validationOf(_google_objects._gmap_element)){

        var _google_LatLng = new google.maps.LatLng(_google_objects._gmap_lat, _google_objects._gmap_lng);
        var _google_map = new google.maps.Map(
            document.getElementById(_google_objects._gmap_element),
            {
                // options for google map
                zoom: (validationOf(_google_objects._gmap_zoom) && _google_objects._gmap_zoom > 0) ? _google_objects._gmap_zoom : 15,
                center: _google_LatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        );

        if(validationOf(_google_objects._gmap_marker)){
            var _based_marker = new google.maps.Marker({
                position: _google_LatLng,
                map: _google_map,
                icon: _google_objects._gmap_marker
            });
        } else {
            var _based_marker = new google.maps.Marker({
                position: _google_LatLng,
                map: _google_map
            });
        }

        if(validationOf(_google_objects._gmap_balloon)){
            google.maps.event.addListener(_based_marker, 'click', function(){
                infowindow = new google.maps.InfoWindow({
                    content: _google_objects._gmap_balloon
                });
                infowindow.open(_google_map, _based_marker);
            });
        }
    }
};

// load google maps
var script = document.createElement("script");
_gmap_api = (validationOf(_google_objects._gmap_api)) ? '?key=' + _google_objects._gmap_api + '&' : '?';
script.type = "text/javascript";
script.src = "http://maps.googleapis.com/maps/api/js" + _gmap_api + "sensor=false&callback=initialize";
document.body.appendChild(script);