/**
 * 
 * @author Donatas Navidonskis <donatas.navidonskis@gmail.com>
 * @package mysite
 *
 * FontAwesome SS Plugin to choose icon for menu
*/


// main functions
(function($){
    $.entwine(function($){

        $("#Root_Main").entwine({
            // constructor
            onmatch: function(){

                initFaSSPlugin();
            }
        });

    });

    var initFaSSPlugin = function(){

        $("#select-fa-icon").fancybox({
            'overlayShow'       : true,
            'scrolling'         : 'visible',
            'mouseWheel'        : true,
            beforeShow: function(){
             $("body").css({'overflow-y':'scroll'});
            },
            afterClose: function(){
             $("body").css({'overflow-y':'hidden'});
            }
        });

        $("body").on("click", ".col-md-3", function(event){
            event.preventDefault();

            $("#fancybox-close").click();

            $("#Form_EditForm_FAIcon").val(
                $.trim(
                    $(this).text()
                )
            ).change();

            $("#Form_EditForm_action_publish")
                .addClass("ss-ui-action-constructiv");

        });

        $("body").on("click", "#remove-font-awesome-icon", function(event){
            event.preventDefault();

            $("#Form_EditForm_FAIcon").val('').change();
            $("#current-icon").remove();
            $(this).remove();
        });

    };
}(jQuery));