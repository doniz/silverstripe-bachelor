<?php

global $project;
$project = 'mysite';

defined('APPLICATION_ENV') || define('APPLICATION_ENV', str_replace(array('ing', 'elopment'), '', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'live')));

Director::set_environment_type(APPLICATION_ENV);

global $databaseConfig;
$_databaseConfig['dev'] = array(
	"type" => 'MySQLDatabase',
	"server" => '127.0.0.1',
	"username" => 'root',
	"password" => 'donatas', // donatas
	"database" => 'bachelor.local',
	"path" => '',
);

$_databaseConfig['test'] = array(
	"type" => 'MySQLDatabase',
	"server" => '127.0.0.1',
	"username" => '',
	"password" => '',
	"database" => '',
	"path" => '',
);
$_databaseConfig['live'] = array(
	"type" => 'MySQLDatabase',
	"server" => '127.0.0.1',
	"username" => 'doniz_bachelor',
	"password" => '2Xo1kF4h',
	"database" => 'doniz_bachelor',
	"path" => '',
);

if (file_exists(realpath(dirname(__FILE__) . '/nolocaldb'))) {
	$_databaseConfig['dev'] = $_databaseConfig['test'];
	$_databaseConfig['dev']['server'] = '127.0.0.1';
}
$databaseConfig = $_databaseConfig[Director::get_environment_type()];

$errorEmail = 'donatas.navidonskis@gmail.lt';
if (file_exists(realpath(dirname(__FILE__) . '/error_email'))) {
	$errorEmail = trim(file_get_contents(realpath(dirname(__FILE__) . '/error_email')));
}
SS_Log::add_writer(new SS_LogEmailWriter($errorEmail), SS_Log::WARN, '<=');
if (Director::isDev() || Director::isTest()) {
	Security::setDefaultAdmin('sysadmin@doniz.net', 'sysadmin');
	// SSViewer::flush_template_cache();
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	// SS_Cache::set_cache_lifetime('any', -1, 666);

	if(class_exists('Memcache')){
		SS_Cache::add_backend(
		    'primary_memcached', 
		    'Memcached',
		    array(
		        'host' => '127.0.0.1',
		        'port' => 11211, 
		        'persistent' => true, 
		        'weight' => 1, 
		        'timeout' => 5,
		        'retry_interval' => 15, 
		        'status' => true, 
		        'failure_callback' => '' 
		    )
		);
		SS_Cache::pick_backend('primary_memcached', /*'any'*/86400, 10);
	}

} else if (Director::isLive()) {
	ini_set('display_errors', 0);
    SS_Cache::set_cache_lifetime('any', 86400, 666);
	error_reporting(0);
}

MySQLDatabase::set_connection_charset('utf8');

// Set the current theme. 
SSViewer::set_theme('custom');
Deprecation::notification_version('3.1.0-dev');

// custom css for amdin theme
LeftAndMain::require_css('mysite/adminlogin/my_screen.css');

// Set the site locale
Translatable::set_default_locale('lt_LT');
Translatable::set_allowed_locales(array('lt_LT', 'en_US', 'ru_RU'));

TranslationCollector::setCollectableModules(
	array('contacts', 'themes/custom', 'google-maps', 'mysite',
		  'langed', 'memberprofiles', 'topics', 'cms', 'framework')
);

// Enable nested URLs for this site (e.g. page/sub-page/)
if (class_exists('SiteTree')) SiteTree::enable_nested_urls();

FulltextSearchable::enable(array('SiteTree'));

HtmlEditorConfig::get('cms')->setOption('forced_root_block', 'p');

Object::add_extension('SiteConfig', 'MySiteConfig');
Object::add_extension('Widget', 'Translatable'); 
Object::add_extension('WidgetArea', 'Translatable');
Object::add_extension('Widget', 'WidgetExtension');

// image quality
GD::set_default_quality(100);


// Google maps extensions
//Object::add_extension("SiteConfig", "GMapSettings");
ContactsPage::add_extension('GMapExtension');


CMSBatchActionHandler::register('Translate', 'CMSBatchActionTranslate');
CMSBatchActionHandler::register('TranslateAndPublish', 'CMSBatchActionTranslateAndPublish');

/**
 * Set FrontPage class to default page of site
 * @var const boolean
 * FRONTPAGE_DEFAULT_PAGE
 *
 * @TRUE: to enable feature
 * @FALSE: to disable feature
 *
*/
define('FRONTPAGE_DEFAULT_PAGE', true);

/**
 * Remove URLSegment fields from CMS
 * @var const boolean
 * FRONTPAGE_REMOVE_CMS_URLSEGMENT
 *
 * @TRUE: to enable feature
 * @FALSE: to disable feature
 *
*/
define('FRONTPAGE_REMOVE_CMS_URLSEGMENT', true);

if( class_exists("FrontPage") && FRONTPAGE_DEFAULT_PAGE){ // check if exist front page

	/**
	 * Stop creating default page with URLSegment /home
	 * @static SiteTree::set_create_default_pages
	 *
	*/
	SiteTree::set_create_default_pages(false); // to allowed set true

	// get current locale if exist Translatable library
	$Locale = ( class_exists("Translatable") ) ? Translatable::get_current_locale() : "lt_LT";

	/**
	 * Set default home page link ( URLSegment )
	 * @static RootURLController::set_default_homepage_link
	 *
	*/
	RootURLController::set_default_homepage_link( // set default home page link

		substr($Locale, 0, 2) // generate prefix of locale ex: lt_LT -> lt

	);


}

/**
 * Dublicate dataobject entries 
 * while is translating a page
 * @var const | boolean
 *
*/
define('DUBLICATE_DATAOBJECT_SET', true);
if( DUBLICATE_DATAOBJECT_SET ){	
	// set dataobjects to dublicate if is page translating
	Page::$_dataobjects = array(
		// "AviaSlider", "TopIcons", "FlyPrices", "HotelPrices" // set DataObject class names
	);
}