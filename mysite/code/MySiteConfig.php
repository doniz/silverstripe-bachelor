<?php
/**
 * @package mysite
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * SiteConfig extension {@link DataObject}.
 * 
 */
class MySiteConfig extends DataExtension {
     
    static $db = array(
        'SearchResultsPerPage' => 'Int',
        'DefaultAdminEmail' => 'Varchar(255)',
        // 'GoogleAnalyticsTrackingID' => 'Varchar(255)',
        'LoginSessionExpireInMinutes' => 'Int'
    );
    
    static $defaults = array(
        'SearchResultsPerPage' => 20,
        'LoginSessionExpireInMinutes' => 30
    );
 
    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab('Root.Main', array(
            new TextField('DefaultAdminEmail', 
                _t('Config.DefaultAdminEmail', 'Default admin email')),
            new TextField('SearchResultsPerPage', 
                _t('Config.SearchResultsPerPage', 'Search results per page')),
            // $ga = new TextField('GoogleAnalyticsTrackingID', 
                // _t('Config.GoogleAnalyticsTrackingID', 'Google Analytics Tracking ID')),
            $SessionExpire = new NumericField('LoginSessionExpireInMinutes',
                _t('Config.LoginSessionExpireInMinutes', 'Login Session Expire (in minutes)'))
            )
        );
        // $ga->setRightTitle(_t('Config.GoogleAnalyticTrackingIDExample', 'UA-XXXXXXXX-XX'));
        $SessionExpire->setRightTitle(_t('Config.LoginSessionExpireInMinutesExplain', 'Session login expiration (in minutes), if member action is inactivity per those minutes automatically login out.'));

        $fields->removeByName('Theme');
        $fields->removeByName('Tagline');
    }
    
    public function DomainName() {
        return $_SERVER['HTTP_HOST'];
    }
}