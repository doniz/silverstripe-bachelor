<?php
/**
 * 
 * @author Donatas Navidonskis (donatas@idea.lt)
 * @license [Ltd Born In Idea] o~
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 *
 * @package BetterImage
 *
 * Better image is object class for extending Image.
 * this class showing image for better quality.
*/
class BetterImage extends Image {

    /**
     * Extending set width function
     * @param int $width
     * @return int $width
     *
    */
    public function SetWidth($width){
        if($width == $this->getWidth()){
            return $this;
        }
        return parent::SetWidth($width);
    }

    /**
     * Extending set height function
     * @param int $height
     * @return int $height
     *
    */
    public function SetHeight($height){
        if($height == $this->getHeight()){
            return $this;
        }
        return parent::SetHeight($height);
    }

    /**
     * Set ratio size of width and height by center of image 
     * @param int $width
     * @param int $height
     * @return int ( ratio size )
     *
    */
    public function SetRatioSize($width, $height){
        if($width == $this->getWidth() && $height == $this->getHeight()){
            return $this;
        }
        return parent::SetRatioSize($width, $height);
    }

    /**
     * Get formatting image
     * @param string $format
     * @param int $arg1
     * @param int $arg2
     *
    */
    public function getFormattedImage($format, $arg1 = null, $arg2 = null) {
        if($this->ID && $this->Filename && Director::fileExists($this->Filename)) {
            $size = getimagesize(Director::baseFolder() . '/' . $this->getField('Filename'));
            $preserveOriginal = false;
            switch(strtolower($format)){
                case 'croppedimage':
                    $preserveOriginal = ($arg1 == $size[0] && $arg2 == $size[1]);
                    break;
            }
             
            if($preserveOriginal){
                return $this;
            } else {
                return parent::getFormattedImage($format, $arg1, $arg2);
            }
        }
    }
}