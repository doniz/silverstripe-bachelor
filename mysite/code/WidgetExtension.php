<?php
class WidgetExtension extends DataExtension { 
   function onBeforeWrite() { 
      parent::onBeforeWrite();

      // Set locale from the currently choosen language locale 
      if($this->owner->Locale != Translatable::get_current_locale()) { 
         $this->owner->Locale = Translatable::get_current_locale(); 
      } 
   } 
}