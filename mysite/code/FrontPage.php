<?php
/**
 * @package mysite
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * FrontPage (home page)
 * 
 */
class FrontPage extends Page {
	
	private static $db = array(

	);

	private static $has_one = array(
		'FrontPageWidgetArea' => 'WidgetArea',
	);
    
    private static $has_many = array(
		// 'Slides' => 'Slide',
	);
    
    //private static $icon = "mysite/treeicons/home-file.gif";
    private static $icon = "mysite/treeicons/home-icon.png";

	public function SubnavigationHeader() {
		return true;
	}

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
        //- Slider
  //       $config = GridFieldConfig_RelationEditor::create();
		// // Set the names and data for our gridfield columns
		// $config->getComponentByType('GridFieldDataColumns')->setDisplayFields(array(
		// 	'Title' => 'Title',
		// 	'Thumbnail' => 'Slide',
		// ));
		// $config->addComponent(new GridFieldSortableRows('SortOrder'));
		// // Create a gridfield to hold the student relationship    
		// $gridField = new GridField(
		// 		'Slides', // Field name
		// 		'Slide', // Field title
		// 		$this->Slides(), // List of all related students
		// 		$config
		// );
		// // Create a tab named "Students" and add our field to it
		// $fields->addFieldToTab('Root.Slider', $gridField);
        
		$fields->addFieldToTab("Root.Valdikliai", new WidgetAreaEditor("FrontPageWidgetArea"));
		
        $fields->removeByName('Widgets');
        //$fields->removeByName('SidebarWidgetArea');
		$fields->removeByName('Content', true);

		if( FRONTPAGE_DEFAULT_PAGE && FRONTPAGE_REMOVE_CMS_URLSEGMENT ){ // if enabled feature

			$fields->removeByName("URLSegment");
			$fields->removeByName("URLSegment_original");

		}
		
		return $fields;
	}

	public function onBeforeWrite(){

		if( FRONTPAGE_DEFAULT_PAGE ){ // if enabled feature

			// get current locale if exist Translatable library
			$Locale = ( class_exists("Translatable") ) ? Translatable::get_current_locale() : "lt_LT";

			// set default url segment
			$this->URLSegment = substr($Locale, 0, 2);

		}

		parent::onBeforeWrite();
	}

}

class FrontPage_Controller extends Page_Controller {
	
    
}