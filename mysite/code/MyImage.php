<?php
/**
 * @package mysite
 * @license www.idea.lt
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @link www.idea.lt
 * @version 1.0
 * 
 * Custom Image
 * 
 */
class MyImage extends Image {
	public function generateGrayscaleCropped(GD $gd, $width, $height) {
		$gd = $gd->croppedResize($width, $height);
		return $gd->greyscale();
	}
    
    /**
     * Resize image if needed
     * 
     */
    public function ResizeImageRatio($width, $height) {
        if($this->getWidth() > $width || $this->getHeight() > $height) {
            //return $image->ResizeRatio($width, $height);
            return $this->SetRatioSize($width, $height);
        }
        return $this;
	}
    
    /**
     * If image is bigger, set width
     * 
     * @param type $width
     * @return \MyImage
     */
    public function SetMaxWidth($width){
        if($this->getWidth() > $width) {
            return parent::SetWidth($width);
        }
        return $this;
    }
    
    
    // this function creates the thumnail for the summary fields to use
	public function getImageThumbnail() {
		return $this->CMSThumbnail();
	}
}