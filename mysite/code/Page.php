<?php
/**
 * @package mysite
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * Page (Base page)
 * 
 */
class Page extends SiteTree {
	
	private static $db = array(
		'ShowInFooterMenu' => 'Boolean',
        // 'InheritWidgets' => 'Boolean',
        'FAIcon' => 'Varchar(40)',
        'DescriptionAboutPage' => 'Varchar(255)'
	);

	private static $has_one = array(
		// 'SidebarWidgetArea' => 'WidgetArea'
	);
    
    private static $defaults = array(
        // 'InheritWidgets' => 1,
        'ShowInFooterMenu' => 0
    );
    
    // allow only Page, OffersHolder, NewsHolder, ContactPage and SimpleGalleryPage children
    // private static $allowed_children = array(
    //     '*Page', 'OffersHolder', 'NewsHolder', 'ContactsPage', 'SimpleGalleryPage', 'FaqPage', 'ProductCatalogPage'
    // );

    /**
     * set of dataobject type (class name) to translate/dublicate
     * while is translating to other language.
     * @var array | null
     * @static $_dataobjects
     *
    */
    public static $_dataobjects;

    /**
     * Simple support for Translatable, when a page is translated, copy all content
     * blocks and relate to translated page.
     * @param none
     * @return void
     *
    */
    public function onTranslatableCreate() {

        if( DUBLICATE_DATAOBJECT_SET && is_array(self::$_dataobjects) && count(self::$_dataobjects) > 0){
            $_translate_page = $this->owner;
            $_original_page  = $this->owner->getTranslation( $this->owner->default_locale() );

            foreach(self::$_dataobjects as $object){
                // check if exist DataObject
                if( DataObject::get_one($object) ){
                    // define of basic silverstripe properties to inherit DataObject set
                    $_ss_objects = array("has_one", "has_many", "many_many");
                    foreach( $_ss_objects as $_ss_object){
                        // check php version
                        if (version_compare(PHP_VERSION, '5.3.0') >= 0){
                            // access private property of class
                            $_reflection_class = new ReflectionClass(get_class($_original_page));

                            if( !$_reflection_class->hasProperty($_ss_object) ) break;

                            $_reflection_property = $_reflection_class->getProperty($_ss_object);                  
                            // set to access property
                            $_reflection_property->setAccessible(true);
                            // getting array of DataObject set
                            $_dataobject_data = $_reflection_property->getValue( $_original_page );
                            // check if exist DataObject in page class
                            if( is_array($_dataobject_data) && in_array($object, $_dataobject_data) ){
                                $object = array_search($object, $_dataobject_data); // get key by value
                                foreach( $_original_page->{$object}() as $_original_object ){
                                    // dublicate all entries from DataObject
                                    $_object = $_original_object->duplicate(true);
                                    // add objects entries to translating page
                                    $_translate_page->{$object}()->add($_object);

                                }
                            }

                        }
                    }
                }
            }

        }

    }

function get_server_load($return_integer = false)
{

    $serverload = array();

    // DIRECTORY_SEPARATOR checks if running windows
    if(DIRECTORY_SEPARATOR != '\\')
    {
        if(function_exists("sys_getloadavg"))
        {
            // sys_getloadavg() will return an array with [0] being load within the last minute.
            $serverload = sys_getloadavg();
            $serverload[0] = round($serverload[0], 4);
        }
        else if(@file_exists("/proc/loadavg") && $load = @file_get_contents("/proc/loadavg"))
        {
            $serverload = explode(" ", $load);
            $serverload[0] = round($serverload[0], 4);
        }
        if(!is_numeric($serverload[0]))
        {
            if(@ini_get('safe_mode') == 'On')
            {
                return "Unknown";
            }

            // Suhosin likes to throw a warning if exec is disabled then die - weird
            if($func_blacklist = @ini_get('suhosin.executor.func.blacklist'))
            {
                if(strpos(",".$func_blacklist.",", 'exec') !== false)
                {
                    return "Unknown";
                }
            }
            // PHP disabled functions?
            if($func_blacklist = @ini_get('disable_functions'))
            {
                if(strpos(",".$func_blacklist.",", 'exec') !== false)
                {
                    return "Unknown";
                }
            }

            $load = @exec("uptime");
            $load = explode("load average: ", $load);
            $serverload = explode(",", $load[1]);
            if(!is_array($serverload))
            {
                return "Unknown";
            }
        }
    }
    else
    {
        return "Unknown";
    }

    $returnload = trim($serverload[0]);

    return ($return_integer) ? (int) $returnload :  number_format($returnload, 2);
}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

        // $fields->addFieldToTab("Root.Widgets", new CheckboxField("InheritWidgets", _t('Page.INHERIT_WIDGETS', 'Inherit Widgets From Parent')));
        // $fields->addFieldToTab("Root.Widgets", new WidgetAreaEditor("SidebarWidgetArea"));

        // Requirements::javascript("mysite/javascript/boxer/jquery.fs.boxer.js");
        // Requirements::css("mysite/javascript/boxer/jquery.fs.boxer.css");

        // Requirements::css("themes/custom/scripts/vendor/bootstrap/dist/css/bootstrap.min.css");
        Requirements::css("themes/custom/scripts/vendor/bootstrap/dist/css/custom_cms.css");

        // Requirements::css("themes/custom/scripts/vendor/bootstrap-jasny/dist/extend/css/jasny-bootstrap.min.css");
        Requirements::css("themes/custom/scripts/vendor/font-awesome/css/font-awesome.css");

        Requirements::css("mysite/javascript/fancybox/jquery.fancybox-1.3.4.css");
        // Requirements::javascript("mysite/javascript/fancybox/jquery.mousewheel-3.0.4.pack.js");
        Requirements::javascript("mysite/javascript/fancybox/jquery.fancybox-1.3.4.pack.js");
        Requirements::javascript("mysite/javascript/faSSplugin.js");


        $RemoveWithIcon = (!empty($this->FAIcon)) ? '<a href="#" id="remove-font-awesome-icon" class="action ss-ui-button ui-button ui-widget ui-state-default ui-button-text-icon-primary ui-corner-right">' . _t("Page.RemoveFontAwesomeIcon", "Pašalinti įkonėlę") . '</a><i id="current-icon" class="fa fa-2x fa-fw '.$this->FAIcon.'"></i>' : '';

        $FontAwesome = null;

        // add font awesome content
        $File = $_SERVER['DOCUMENT_ROOT'] . "/themes/custom/templates/custom-pages/FontAwesome.html";

        if(file_exists($File)){
            $FontAwesome = file_get_contents($File);
        }

        // Server CPU Usage
        // var_dump($this->get_server_load());

        $fields->addFieldsToTab("Root.Main",
            array(
                LiteralField::create('FAIconSectionStart',
                '<div id="FAIconSectionStart" class="field text">
                    <label class="left" for="select-fa-icon">'._t("Page.SelectFontAwesomeIcon", "Pasirinkite menių įkonėlę").'</label>
                    <div class="middleColumn">
                        <a href="#hidden-font-awesome" class="action ss-ui-button ui-button ui-widget ui-state-default ui-button-text-icon-primary ui-corner-right" id="select-fa-icon">'._t("Page.SelectFontAwesomeIconButton", "Rinktis").'</a>
                        &nbsp;&nbsp;
                        '.$RemoveWithIcon.'
                    </div>
                    <div style="display:none;">
                        <div id="hidden-font-awesome" style="width:650px;margin-left:50px;height:500px;overflow-y:scroll;overflow-x:hidden;">
                        
                        '.$FontAwesome.'

                        </div>
                    </div>
                </div>'
                ),
                HiddenField::create("FAIcon", null, $this->FAIcon)
            ),
            "Content"
        );

        $fields->addFieldToTab("Root.Main",
            TextField::create(
                "DescriptionAboutPage",
                _t("Page.DESCRIPTION_ABOUT_PAGE", "Trumpas aprašymas apie puslapį. (Rodomas šalią pavadinimo)")
            ),
            "Content"
        );

		
		return $fields;
	}

    public function isMemberLoggedIn(){
        return Member::currentUserID();
    }

    public function CurrentMember(){
        if($this->isMemberLoggedIn()){
            return Member::currentUser();
        }
    }
        
    public function FrontEndProfileImage(){
        $Member = $this->CurrentMember();

        if($Member->ProfileImageID > 0){
            $ProfileImage = singleton("Image")->get()->filter(array("ID" => $Member->ProfileImageID));

            // Debug::dump($Member->ProfileImage());
            // exit;

            return $ProfileImage->First();
        }
    }

    /**
	 * In Settings tab added ShowInFooterMenu
     * 
	 * @return FieldList
	 */
    public function getSettingsFields() { 
      $fields = parent::getSettingsFields(); 
      $fields->addFieldToTab("Root.Settings", new CheckboxField('ShowInFooterMenu', _t('Page.ShowInFooterMenu', 'Show in Footer Menu')), 'ShowInSearch');
      return $fields; 
   }
   
   /**
    * TURBO :)
	* Return the link for this {@link SiteTree} object, with the {@link Director::baseURL()} included.
	*
	* @return string
	*/
	public function CustomLink($object=null) {
        
        if(!$object) $object = $this;
        
        if(empty($object->Content) && $object->NeedRedirectIfNoContent() && $object->Children()->Count()) {
            $children = $object->Children()->First();
            return $this->CustomLink($children);
        }
        return Controller::join_links(Director::baseURL(), $object->RelativeLink());
	}
    
    /*
     * If page has'nt content - need to redirect to first child
     * 
     * @return boolean
     */
    public function NeedRedirectIfNoContent() {
        return true;
    }
    
    /**
     * Widget inheritance
     * 
     * @param none
     * @return object | WidgetArea
     */
    public function SidebarWidgetArea() {
        
        if($this->InheritWidgets && $this->getParent()) {
            if (method_exists($this->getParent(), 'SidebarWidgetArea')) return $this->getParent()->SidebarWidgetArea();
        }
        if($this->SidebarWidgetAreaID){
            return DataObject::get_by_id('WidgetArea', $this->SidebarWidgetAreaID);
            // @todo: This segfaults - investigate why then fix: return $this->getComponent('SidebarWidgetArea');
        }
	}

	public function AbsoluteHomeURL() {
		return Director::absoluteURL("/", true);
	}
	
    /**
     * Return Home Page Link
     * 
     * @return string URLSegment
     */
	public function LinkHomepageCurrLang() {
        $homepage = Translatable::get_one_by_locale('FrontPage', Translatable::get_current_locale());
        if(isset($homepage) && $homepage->isPublished()) {
            return $homepage->URLSegment;
        }
        return false;
	}
	
    /**
     * Is this a home page?
     * @return boolean
     */
	public function IsHomepage() {
		return $this->LinkHomepageCurrLang() == $this->URLSegment || $this->URLSegment == 'FrontPage_Controller';
	}
	
    
	public function ParentLink() {
		return $this->Parent()->Link();
	}
	
    /**
	 * Generate a URL segment based on the title provided.
     * 
     * Added Foreign character conversion to Latin
	 * 	 
	 * @param string $title Page title.
	 * @return string Generated url segment
	 */
	public function generateURLSegment($title) {
		$title = $this->ForeignToLatin($title);
		return parent::generateURLSegment($title);
	}

    private static function get_existing_frontpage_locales(){
        if(class_exists("Translatable") && class_exists("FrontPage")){
            $available_languages = Translatable::get_existing_content_languages('FrontPage');

            // get array keys from $available_languages (key's is a locales)
            $available_locales = array_keys($available_languages);

            return (count($available_locales) > 0) ? $available_locales : null;
        }

        return null;
    }
    
    /*
     * Language menu consisting only of published FrontPage pages
     * 
     * @param none
     * @return ArrayList
     */
    public function LanguageMenu() {
        
        $db = DB::getConn();
        
        $allFrontPages = $db->query("SELECT DISTINCT \"Locale\", URLSegment, ID FROM \"SiteTree\" WHERE \"ClassName\"='FrontPage'");

        $originalLocale = Translatable::get_current_locale();
        $allowedLocales = self::get_existing_frontpage_locales();

        $langs = new ArrayList();
        
        foreach( $allFrontPages as $item ) { 
            // check if this Locale is published
            Translatable::set_current_locale($item['Locale']);
            $tmpLocale = Translatable::get_current_locale();
            $frontPage = FrontPage::get_by_id('FrontPage', $item['ID']);
            if($frontPage && in_array($item['Locale'],$allowedLocales)) {
                $current = ($item['Locale'] == $originalLocale);
                $xploded = explode('_', $item['Locale']);
                $arrayData = new ArrayData(array(
                         'Locale' => $frontPage->Locale, 
                         'Link' => $frontPage->Link(),
                         'Current' => $current,
                         'Title' => strtoupper($xploded[0])
                     )        
             );        
             $langs->add($arrayData);     
            }
        }
        // set back original locale
        Translatable::set_current_locale($originalLocale);
        return ($langs->Count() > 1) ? $langs : false;
        
	}
	
    /**
     * Website available locales
     */
	public function WebsiteLocales() {
		$allowed = Translatable::get_allowed_locales();
		$result = new ArrayList();
		foreach ($allowed as $locale) {
			$xploded = explode('_', $locale);
			$result->push(array(
				'Title' => ucfirst($xploded[0]),
				'Locale' => $locale,
				'Link' => '?locale=' . $locale,
				'Current' => Translatable::get_current_locale() == $locale
			));
		}
		return $result;
	}
	
	/**
     * Convert Foreign symbols to Latin
     * 
     * @param string $str.
     * @return string with only Latin Symbols
     */
    public function ForeignToLatin($str) {
        $tr = array(
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g", "Д" => "d",
            "Е" => "e", "Ё" => "yo", "Ж" => "zh", "З" => "z", "И" => "i",
            "Й" => "j", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "kh", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "y", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "yo",
            "ж" => "zh", "з" => "z", "и" => "i", "й" => "j", "к" => "k",
            "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p",
            "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f",
            "х" => "kh", "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch",
            "ъ" => "", "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu",
            "я" => "ya", "ą" => "a", "č" => "c", "ę" => "e", "ė" => "e",
            "į" => "i", "š" => "s", "ų" => "u", "ū" => "u", "ž" => "z"
        );
        return strtr($str, $tr);
    }
    
    
	/*
     * ? TODO - kur naudojama? Ar reikia?
     */
	public function Puslapiu($num) {
		$result = '';
		$lastDigit = $num % 10;
		if ($lastDigit == 1) {
			$result = 'is';
		} else if ($lastDigit == 0) {
			$result = 'ių';
		} else if ($lastDigit > 1 && $lastDigit <= 9) {
			$result = 'iai';
		}
		$last2Digits = $num % 100;
		if ($last2Digits > 10 && $last2Digits < 20) {
			$result = 'ių';
		}
		return 'puslap' . $result;
	}
    
    public function PartnersList() {
        if(class_exists('Partner')) {
            return Partner::get();
        }
        return false;
    }
	

	public function PartnersSorted() {
		$partnersPage = DataObject::get('PartnersPage', 'Locale = \'' . Translatable::get_current_locale() . '\'')->First();
        if (method_exists($partnersPage, 'Partners')) {
			return $partnersPage->Partners()->Sort('SortOrder');
		}
		return null;
	}

    /**
     * Function is for caching to flush new records
     * @param none
     * @return none
     *
    */
    public function onAfterWrite(){
        parent::onAfterWrite();

        if( Director::isLive() ){
            SSViewer::flush_template_cache();
        }
    }
    
    /**
     * Return Link to current Class Page
     * 
     * @param type $Class
     * @return $Link string | false
     */
    public function SelectedPageLink($Class='SearchPage') {
        
        if($Class && class_exists($Class)) {
            $Page = $Class::get_one($Class);
            return ($Page) ? $Page->Link() :  false;
        }
        return false;
    }

	
	/**
     * Get Final Parent page
     * 
     * @param object $page
     * @return object Page
     */
	public function FinalParent($page = null) {
		if (!$page) {
			$page = $this;
		}
		if ($page->ParentID > 0) {
			return $this->FinalParent($page->Parent());
		} else {
			return $page;
		}
	}
    
    /**
     * Return fixed external link
     * 
     * @param string $Link
     * @param string $scheme
     * @return boolean
     */
    public function fixLink($Link, $scheme = 'http://') {
        if($Link) {
            if (parse_url($Link, PHP_URL_SCHEME) === null) {
                return $scheme . $Link;
            }
            return $Link;
        }
        return false;
    }
    
    /**
     * Graphic element for page. If page don't have, get from parent recursively
     * 
     * @param object $page
     * @return Image object | false
     */
    public function GraphicElement($page = null) {     
		if (!$page) {
			$page = $this;
		}
        if(!$page->ImageID && !$page->ParentID) 
            return false;
        
		if (!$page->ImageID || !$page->Image()->ID) {
			return $this->GraphicElement($page->Parent());
		} else {
			return $page->Image();
		}
        
	}
    
    /**
     * Resize image if needed
     * 
     */
    public function ResizeImageRatio($width, $height) {
        if(!$this->ImageID) return false;
        $image = $this->Image();
        if($image->width > $width || $image->height > $height) {
            //return $image->ResizeRatio($width, $height);
            return $image->SetRatioSize($width, $height);
        }
        return $image;
	}
    
    
    /**
	 * Limit field's content by a number of characters.
	 * This makes use of strip_tags() to avoid malforming the
	 * HTML tags in the string of text.
     * 
     * @param string $field by default is Content.
	 * @param int $limit Number of characters to limit by
	 * @param string $add Ellipsis to add to the end of truncated string
	 * @return string
	 */
	public function LimitCharactersForMetaDescription($field = null, $limit = 157, $add = '...') {
		
        $value = ($field) ? $field : $this->Content;
        
        $value = trim(strip_tags($value));
        $value = html_entity_decode($value, ENT_COMPAT, 'UTF-8');
        $value = (mb_strlen($value) > $limit) ? mb_substr($value, 0, $limit) . $add : $value;
        // Avoid encoding all multibyte characters as HTML entities by using htmlspecialchars().
        $value = htmlspecialchars($value, ENT_COMPAT, 'UTF-8');

		return $value;
	}
    
    /**
     * Check if person is logged in
     * 
     * @return boolean
     */
	public function LogedIn() {
		return Member::currentUser() && Member::logged_in_session_exists();
	}
    
    /**
     * Is this Live version? - used for tracking codes ant other stuffs, which 
     * should only be in LIVE version
     * 
     * @return boolean
     */
    public function isLive(){
        return Director::isLive();
    }
    
    public function isDev(){
        return Director::isDev();
    }
    
    public function FacebookImage() {
		return null;
	}

    // function get MemberProfilePage link by locale
    public function GetMemberProfilePage(){
        if(class_exists("Translatable") && class_exists("MemberProfilePage")){
            return singleton("MemberProfilePage")->get()
                    ->filter(array(
                        "Locale" => Translatable::get_current_locale()
                    ))
                    ->First();
        }
    }

    public function GetCurrentMemberGroupName(){
        $Member = Member::currentUser();

        if(!$Member) return false;

        return $this->TranslateGroupById($Member->Groups()->First()->ID);
    }

    public function GetStudentsGroupId(){
        $config = SiteConfig::current_site_config();

        if($config->STUDENT_Group() && $config->STUDENT_Group()->ID){
            return $config->STUDENT_Group()->ID;
        }
    }

    public function isSYSAdmin(){
        $Member = Member::currentUser();

        if(!$Member) return false;

        $config = SiteConfig::current_site_config();

        if($config->SYSADMIN_GROUP() 
            && $config->SYSADMIN_GROUP()->ID){
            $users_groups = array();

            foreach($Member->Groups() as $Group){
                $users_groups[$Group->ID] = $Group->ID;
            }

            return in_array($config->SYSADMIN_GROUP()->ID, $users_groups);
        }

        return false;
    }

    public function isAdmin(){
        $Member = Member::currentUser();

        if(!$Member) return false;

        $config = SiteConfig::current_site_config();

        if($config->ADMIN_GROUP() 
            && $config->ADMIN_GROUP()->ID){
            $users_groups = array();

            foreach($Member->Groups() as $Group){
                $users_groups[$Group->ID] = $Group->ID;
            }

            return in_array($config->ADMIN_GROUP()->ID, $users_groups);
        }

        return false;
    }

    public function isLecturer(){
        $Member = Member::currentUser();

        if(!$Member) return false;

        $config = SiteConfig::current_site_config();

        if($config->LECTURER_GROUP() 
            && $config->LECTURER_GROUP()->ID){
            $users_groups = array();

            foreach($Member->Groups() as $Group){
                $users_groups[$Group->ID] = $Group->ID;
            }

            return in_array($config->LECTURER_GROUP()->ID, $users_groups);
        }

        return false;
    }

    public function isManager(){
        $Member = Member::currentUser();

        if(!$Member) return false;

        $config = SiteConfig::current_site_config();

        if($config->MANAGER_GROUP() 
            && $config->MANAGER_GROUP()->ID){
            $users_groups = array();

            foreach($Member->Groups() as $Group){
                $users_groups[$Group->ID] = $Group->ID;
            }

            return in_array($config->MANAGER_GROUP()->ID, $users_groups);
        }

        return false;
    }

    public function isStudent(){
        $Member = Member::currentUser();

        if(!$Member) return false;

        $config = SiteConfig::current_site_config();

        if($config->STUDENT_GROUP() 
            && $config->STUDENT_GROUP()->ID){
            $users_groups = array();

            foreach($Member->Groups() as $Group){
                $users_groups[$Group->ID] = $Group->ID;
            }

            return in_array($config->STUDENT_GROUP()->ID, $users_groups);
        }

        return false;
    }

    public function GetLecturersGroupId(){
        $config = SiteConfig::current_site_config();

        if($config->LECTURER_Group() && $config->LECTURER_Group()->ID){
            return $config->LECTURER_Group()->ID;
        }
    }

    public function GetLecturers(){
        return singleton("Group")->get()->filter(array("ID" => $this->GetLecturersGroupId()))->First()->DirectMembers()->sort("FirstName", "ASC");
    }

    public function GetStudents(){
        return singleton("Group")->get()->filter(array("ID" => $this->GetStudentsGroupId()))->First()->DirectMembers()->sort("FirstName", "ASC");
    }

    public function getUserById($id){
        return (is_int($id) || is_numeric($id)) ? singleton("Member")->get()->filter(array("ID" => $id))->First() : null;
    }

    public function TranslateGroupById($id = null){
        if(!is_null($id) && (is_numeric($id) || is_int($id))){
            $config  = SiteConfig::current_site_config();

            if($config->SYSADMIN_Group()->ID == $id){
                return $this->TranslateGroupByName("SYSADMIN");
            }

            if($config->MANAGER_Group()->ID == $id){
                return $this->TranslateGroupByName("MANAGER");
            }

            if($config->STUDENT_Group()->ID == $id){
                return $this->TranslateGroupByName("STUDENT");
            }

            if($config->LECTURER_Group()->ID == $id){
                return $this->TranslateGroupByName("LECTURER");
            }

            if($config->ADMIN_Group()->ID == $id){
                return $this->TranslateGroupByName("ADMIN");
            }

            return singleton("Group")
                ->get()
                ->filter(array("ID" => $id))
                ->First()
                ->Title;
        }
    }

    public function TranslateGroupByName($name = "SYSADMIN"){
        switch($name){
            case "SYSADMIN":
                return _t("Page.SYSADMIN_GROUP", "Sistemos administratorius");
            break;

            case "MANAGER":
                return _t("Page.MANAGER_GROUP", "Vedėjas");
            break;

            case "STUDENT":
                return _t("Page.STUDENT_GROUP", "Studentas");
            break;

            case "ADMIN":
                return _t("Page.ADMIN_GROUP", "Administratorius");
            break;

            case "LECTURER":
                return _t("Page.LECTURER_GROUP", "Dėstytojas");
            break;
        }

        return null;
    }

    public function ContactsPageContent(){
        if(!class_exists("ContactsPage")) return false;

        $Page = singleton("ContactsPage")
                ->get()
                ->filter(array(
                    "Locale" => Translatable::get_current_locale()
                ));

        // if exist page in current choosed locale,
        // and if is check ShowContentAtFooter,
        // then return content
        if($Page->Count() > 0 && $Page->First()->ShowContentAtFooter){
            return $Page->First()->Content;
        }

        return false;
    }

    public function TreeChildren(){
        return singleton("SiteTree")->get();
    }
    
}
class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		'SearchForm', 'signout'
	);

    public function signout(){
        $member = Member::currentUser();
        if($member) $member->logOut();

        $this->redirect(
            singleton("FrontPage")
                ->get()
                ->filter(
                    array("Locale" => Translatable::get_current_locale())
                )
                ->First()
                ->Link()
        );
    }

    public function BackURL() { 
        if(isset($_REQUEST['BackURL'])) { 
            return $_REQUEST['BackURL']; 
        } else { 
            return Session::get('BackURL'); 
        }
    }

	public function init() {
		parent::init();
		
        // if user session expire,
        // then loggout him from the system
        self::redirectInactiveUser();

        if(empty($this->Content) && $this->Children()->Count() > 0){
            $this->redirect($this->Children()->First()->URLSegment);
        }

		i18n::set_locale(Translatable::get_current_locale());
		i18n::set_default_locale(Translatable::get_current_locale());
	
	}

    public static function redirectInactiveUser(){
        $Member = Member::currentUser();

        if( $Member ){

            $config = SiteConfig::current_site_config();


            $inactivityLimit = ($config->LoginSessionExpireInMinutes > 0) ? $config->LoginSessionExpireInMinutes : 30; // in Minutes 
            $inactivityLimit = $inactivityLimit * 60; // Converted to seconds 
            $sessionStart = Session::get('session_start_time');

            if(isset($sessionStart)){
                $elapsed_time = time() - Session::get('session_start_time');

                if ($elapsed_time >= $inactivityLimit) {

                    $Member->logOut();
                    Session::clear_all();

                    $Link = singleton("FrontPage")
                            ->get()
                            ->filter(
                                array("Locale" => Translatable::get_current_locale())
                            )
                            ->First()
                            ->Link();

                    ?><script type="text/javascript">window.location.href = "<?php echo $Link;?>";</script><?php

                }
            }

            Session::set("session_start_time", time());

        }

    }

    /**
	 * Site search form
	 */
	public function SearchForm() {
		$f = parent::SearchForm();
		$config = SiteConfig::current_site_config();
		$f->setPageLength($config->SearchResultsPerPage);
		return $f;
	}
	
    /**
	 * Extend site search and generate results.
	 *
	 * @param array $data The raw request data submitted by user
	 * @param SearchForm $form The form instance that was submitted
	 * @param SS_HTTPRequest $request Request generated for this action
	 */
	public function results($data, $form, $request) {
        $data = array(
            'Query' => $form->getSearchQuery(),
            'Title' => _t('SearchForm.SearchResults', 'Search Results')
        );
		
		$data['SearchStarted'] = isset($_REQUEST['Search']);
		$data['SubnavigationHeader'] = false;
		
		$data['EmptyQuery'] = false;
		if ($data['Query'] == '') {
			$data['EmptyQuery'] = true;
		} else {
			$data['Results'] = $form->getResults();	
			$data['PosStart'] = $data['Results']->getPageStart() + 1;
		}

        return $this->owner->customise($data)->renderWith(array('Page_results', 'Page'));
    }
    
    /**
	 * Remove generator=silverstripe tag
	 * Return the title, description, keywords and language metatags
     * 
     * @param Boolean $includeTitle
	 * @return string The XHTML metatags
	 */
	public function MetaTags($includeTitle = true) {
		$tags = "";
		if($includeTitle === true || $includeTitle == 'true') {
			$tags .= "<title>" . $this->Title . "</title>\n";
		}
        
		$charset = Config::inst()->get('ContentNegotiator', 'encoding');
		$tags .= "<meta http-equiv=\"Content-type\" content=\"text/html; charset=$charset\" />\n";
        $tags .= "<meta name=\"author\" content=\"Donatas Navidonskis - www.doniz.net\" />\n";
		if($this->MetaDescription()) {
			$tags .= "<meta name=\"description\" content=\"" . Convert::raw2att($this->MetaDescription()) . "\" />\n";
		}
		if($this->ExtraMeta) { 
			$tags .= $this->ExtraMeta . "\n";
		}
        
        //facebook meta tags
        $tags .= $this->FacebookMetaTags(false);

		if(Permission::check('CMS_ACCESS_CMSMain') && in_array('CMSPreviewable', class_implements($this)) && !$this instanceof ErrorPage) {
			$tags .= "<meta name=\"x-page-id\" content=\"{$this->ID}\" />\n";
			$tags .= "<meta name=\"x-cms-edit-link\" content=\"" . $this->CMSEditLink() . "\" />\n";
		}

		$this->extend('MetaTags', $tags);

		return $tags;
	}
    
    /*
     * Meta description
     */
    public function MetaDescription(){
        if($this->MetaDescription) {
            return $this->MetaDescription;
        } elseif($this->Content) {
           
            return $this->LimitCharactersForMetaDescription();
        }
        return _t('Top.DEFAULT_META', '');
		
	}
	
    /**
	 * Remove generator=silverstripe tag
	 * Return the title, description, url, image metatags
     * 
     * @param Boolean $showtags
	 * @return string The XHTML metatags
	 */
	public function FacebookMetaTags($showtags=true) {
        if($showtags) {
            $tags = '';
            $title = ($this->MetaTitle) ? $this->MetaTitle : $this->Title;  
            $tags .= "<meta property=\"og:title\" content=\"".Convert::raw2att($title)."\" />\n";
            $tags .= "<meta property=\"og:type\" content=\"website\" />\n";
            $tags .= "<meta property=\"og:description\" content=\"".Convert::raw2att($this->MetaDescription())."\" />\n";	
            $tags .= "<meta property=\"og:url\" content=\"".$this->AbsoluteLink()."\" />\n";
            //$tags .= "<meta property=\"og:image\" content=\"".Director::absoluteBaseURL().'assets/img/200x200.png'."\" />\n";
            $tags .= "<meta property=\"og:site_name\" content=\"".$this->SiteConfig()->Title."\" />\n";
            //$tags .= "<meta property=\"fb:app_id\" content=\"\" />";
			
			if (Director::get_current_page()->FacebookImage()) {
				$tags .= "<meta property=\"og:image\" content=\"".$this->FacebookImage()->AbsoluteURL."\" />\n";
			}
			
            return $tags;
        }
        return false;
	}


    
    
}
