<?php
/**
 * @package mysite
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * Slide data objet for slideshow
 * 
 */
class Slide extends DataObject {

	private static $db = array(
		'SortOrder' => 'Int',
		'Title' => 'Text',
        'InnerPage' => 'Boolean',
        'ExternalLink' => 'Varchar(255)'
        
	);
    
	// One-to-one relationship
	private static $has_one = array(
		'Image' => 'Image',
		'FrontPage' => 'FrontPage',
		"LinkTo" => "SiteTree",
	);
    
    private static $default_sort = 'SortOrder';

    private static $defaults = array(
        'InnerPage' => true
    );  
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
        // upload slide in specified folder
		$uploadField = new UploadField('Image', _t($this->class.'.Image', 'Image'));
        $uploadField->setFolderName('Uploads/Slider');
        $fields->addFieldToTab("Root.Main", $uploadField);
        // link to page in SiteTree
		$fields->addFieldsToTab('Root.Main', 
                array(
                    OptionsetField::create("InnerPage", _t($this->class.'.CMS_INNERPAGE', 'Linking type'), array(
                            _t($this->class.'.CMS_CHOOSE_EXTERNALLINK', 'External Link'),
                            _t($this->class.'.CMS_CHOOSE_INNERLINK', 'Link in Site Tree'),
                        ), 1),
                    $siteTree = TreeDropdownField::create('LinkToID', _t($this->class.'.CMS_LINKTOPAGE','Link to inner page'), 'SiteTree'),
                    $external = TextField::create('ExternalLink', _t($this->class.'.CMS_EXTERNALLINK', 'External Link'))
                )    
        );
        
        $siteTree->displayIf('InnerPage')->isEqualTo(1);
        $external->displayIf('InnerPage')->isEqualTo(0);
               
        //hide some fields
        $fields->removeFieldFromTab("Root.Main", "FrontPageID");
		$fields->removeFieldFromTab("Root.Main", "SortOrder");
		
		return $fields;
	}

	// Tell the datagrid what fields to show in the table
	public static $summary_fields = array(
		'Title' => 'Title',
		'Thumbnail' => 'Thumbnail'
	);

	// this function creates the thumnail for the summary fields to use
	public function getThumbnail() {
		return $this->Image()->CMSThumbnail();
	}
    
    public function LinkingExist() {
        return (($this->InnerPage && $this->LinkToID) || (!$this->InnerPage && !empty($this->ExternalLink)));   
    }
    
    /**
     * Return fixed external link
     * 
     * @param string $Link
     * @param string $scheme
     * @return boolean
     */
    public function fixLink($Link, $scheme = 'http://') {
        if($Link) {
            if (parse_url($Link, PHP_URL_SCHEME) === null) {
                return $scheme . $Link;
            }
            return $Link;
        }
        return false;
    }

}