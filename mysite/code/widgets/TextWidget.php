<?php
/**
 * @package mysite
 * @author Neringa Rigertaite <neringa@idea.lt>
 * @copyright 2013 IDEA [Ltd Interaktyvi reklama]
 * @license IDEA
 * @link www.idea.lt
 * @since 2013
 * @version 1.0
 * 
 * Text Widget - simple HTML content block
 * 
 */
class TextWidget extends Widget {

	private static $title = 'HTML Content widget';
	private static $cmsTitle = 'HTML Content widget';
	private static $description = 'Widget with HTML content';
    
	private static $db = array(
		'WidgetContent' => 'HTMLText',
	);
	private static $has_one = array(
	);

	public function getCMSFields() {
        return new FieldList(
                    HtmlEditorField::create('WidgetContent', _t('TextWidget.Content', 'Content'))
                                    ->setRows(15)
                );
	}

   
}

class TextWidget_Controller extends Widget_Controller {

}