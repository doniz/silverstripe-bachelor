<?php

class RegistrationSuccess extends Page {

	private static $defaults = array(
		"ShowInMenus" => false,
		"ShowInFooterMenu" => false,
		"ShowInSearch" => false,
		"CanViewType" => "Anyone"
	);

}

class RegistrationSuccess_Controller extends Page_Controller {

}