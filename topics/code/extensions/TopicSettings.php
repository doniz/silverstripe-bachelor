<?php


class TopicSettings extends DataExtension {
     
    static $db = array(
        "GlobalExpireFrom"    => "Date",
        "GlobalExpireTo"      => "Date",
    );
    
    static $defaults = array(

    );

    static $has_one = array(

    );
 
    public function updateCMSFields(FieldList $fields){
        $fields->addFieldToTab('Root', TabSet::create('Topics', _t('TopicSettings.Topics', 'Tematikų nustatymai')));
        $fields->addFieldsToTab('Root.Topics', array(
            Tab::create(
                'ExpiryDates',
                _t('TopicSettings.ExpiryDates', 'Galiojimo datos')

            )
        ));
    }

}