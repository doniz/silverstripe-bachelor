<?php
/**
 *
 * @author Donatas Navidonskis <donatas.navidonskis@gmail.com>
 * @license Donatas Navidonskis
 * @link www.doniz.net
 * @package topics
 * @version 1.0
 *
 * Topic page accessing by managers or admin
 * to add topic points for lectures/teachers.
 *
*/

class TopicPage extends Page implements PermissionProvider {

	/**
	 * 
	 * Database objects/table fields/variables
	 *
	 * @var array
	 * @static $db
	 *
	*/
	private static $db = array(
		"ItemsPerPage" => "Int"
	);

	/**
	 * 
	 * array for DataObjects to initialize.
	 * One to one relationships with current page.
	 *
	 * @var array
	 * @static $has_one
	 *
	*/
	private static $has_one = array(

	);

	/**
	 * 
	 * array for DataObjects to initialize.
	 * One to many relationships with current page.
	 *
	 * @var array
	 * @static $has_many
	 *
	*/
	private static $has_many = array(
		// "TopicAmountObject" => "TopicAmountObject"
	);

	/**
	 *
	 * default $db values
	 *
	 * @var array
	 * @static $defaults
	 *
	*/
	private static $defaults = array(
		"ItemsPerPage" => 10
	);

	/**
	 *
	 * Description of current module page
	 * to show at CMS when creating new page.
	 *
	 * @var string
	 * @static $description
	 *
	*/
	private static $description = "Module page to creating topics for lecturers.";

	private static $icon = "mysite/treeicons/item-icon.png";
	
	/**
	 *
	 * CMS data field list (form)
	 *
	 * @param none
	 *
	*/
	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->addFieldToTab("Root.Main", $ItemsPerPage = TextField::create(
			"ItemsPerPage",
			_t("TopicPage.ItemsPerPage", "Įrašų per puslapį")
			), "Content"
		);

		return $fields;
	}

	public function TopicTypes($perPage = 1){
		$TopicTypes = singleton("TopicTypes")
		       ->get()
		       ->filter(array(
		       		"Locale" => Translatable::get_current_locale()
		       	))
		       ->sort("Created", "desc");

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($TopicTypes->toArray()));
        $paginator->setCurrentPageNumber(
        	(isset($_REQUEST["page"])) ? (int) $_REQUEST["page"] : 1
        );


        // if is set
		if ( !empty( $this->ItemsPerPage ) ) {
			
			$perPage = $this->ItemsPerPage;
		
		}

        $paginator->setItemCountPerPage($perPage);

        $data = new ArrayList(
        	iterator_to_array($paginator->getCurrentItems(), false)
        	
        );

		$gettedPages = $paginator->getPages();


		if(is_array($gettedPages->pagesInRange)){
			$temp_pagesInRange = array();
			$pagesInRange = $gettedPages->pagesInRange;
			
			foreach($pagesInRange as $key => $value){
				array_push($temp_pagesInRange, array("item" => $value));
			}

			$gettedPages->pagesInRange = new ArrayList($temp_pagesInRange);
		}

		$data->pages = new ArrayData($gettedPages);

		$data->currentPage = (isset($_REQUEST["page"])) ? (int) $_REQUEST["page"] : 1;

		return $data;
	}

	public function getLinkOfMembersPage(){
		return singleton("MembersPage")->get()->filter(array("Locale" => Translatable::get_current_locale()))->First()->Link();
	}


	public function GetEditTopicTypePage(){
		return singleton("EditTopicTypepage")->get()
		       ->filter(array(
		       		"ParentID" => $this->ID,
		       		"Locale" => Translatable::get_current_locale()
		       	))
		       ->First();
	}

}

class TopicPage_Controller extends Page_Controller {

	/**
	 * 
	 * defining to allowed controller url
	 * for some actions by methods. 
	 *
	 * @var array
	 * @static $allowed_actions
	 *
	*/
	private static $allowed_actions = array(
		'edit_type', 'delete_type', 'add_new_type',
		'TopicTypesForm'
	);

	/**
	 *
	 * initializing controller
	 *
	 * @param none
	 *
	*/
	public function init(){
		parent::init();

	}

	public function add_new_type(){
		if($this->isSYSAdmin() || $this->isAdmin()){

			$data = array(
				"Title" => _t("TopicPage.AddNewType", "Add new topic type"),
				"Form" => new TopicTypesForm($this, "TopicTypesForm")
			);

			return $this->customise($data)
	        ->renderWith(array(
	       		"TopicPage_add_new_type", "Page"
	        ));
		}

		return $this->redirectBack();
	}

	public function edit_type($request){
		if($this->isSYSAdmin() || $this->isAdmin()){
			$data = array(
				"Title" => _t("TopicPage.EditType", "Edit topic type"),
				"Form"  => new TopicTypesForm($this, "TopicTypesForm", $request->param("ID")),
				"data"  => null
			);

			return $this->customise($data)
			->renderWith(array(
				"TopicPage_add_new_type", "Page"
			));
		}

		return $this->redirectBack();
	}

	public function delete_type($request){
		$data = singleton("TopicTypes")->get()->filter(array("ID" => $request->param("ID")));
		
		// delete object if find
		if($data->Count() == 1){
			
			$data->First()->delete();
		}

		// redirect back to page
		return $this->redirectBack();
	}

    // // actions of object
    // public function delete($id = null){
    //     $data = singleton("TopcTypes")->get()->filter(array(
    //         "ID" => $id
    //     ));


    //     if($data->Count() == 1){
    //         $data->First()->delete();
    //         return true;
    //     }

    //     return false;
    // }

	public function TopicTypesForm(){
		$Form = new TopicTypesForm($this, "TopicTypesForm");
		$Form->clearMessage();

		return $Form;
	}

}