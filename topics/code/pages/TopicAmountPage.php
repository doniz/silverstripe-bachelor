<?php
/**
 *
 * @author Donatas Navidonskis <donatas.navidonskis@gmail.com>
 * @license Donatas Navidonskis
 * @link www.doniz.net
 * @package topics
 * @version 1.0
 *
 *
*/

class TopicAmountPage extends Page implements PermissionProvider {

	/**
	 * 
	 * Database objects/table fields/variables
	 *
	 * @var array
	 * @static $db
	 *
	*/
	private static $db = array(
		"ItemsPerPage" => "Int"
	);

	/**
	 * 
	 * array for DataObjects to initialize.
	 * One to one relationships with current page.
	 *
	 * @var array
	 * @static $has_one
	 *
	*/
	private static $has_one = array(

	);

	/**
	 * 
	 * array for DataObjects to initialize.
	 * One to many relationships with current page.
	 *
	 * @var array
	 * @static $has_many
	 *
	*/
	private static $has_many = array(
		// "TopicAmountObject" => "TopicAmountObject"
	);

	/**
	 *
	 * default $db values
	 *
	 * @var array
	 * @static $defaults
	 *
	*/
	private static $defaults = array(
		"ItemsPerPage" => 10
	);

	/**
	 *
	 * Description of current module page
	 * to show at CMS when creating new page.
	 *
	 * @var string
	 * @static $description
	 *
	*/
	private static $description = "Module page to creating topics for lecturers.";

	private static $icon = "mysite/treeicons/item-icon.png";
	
	/**
	 *
	 * CMS data field list (form)
	 *
	 * @param none
	 *
	*/
	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->addFieldToTab("Root.Main", $ItemsPerPage = TextField::create(
			"ItemsPerPage",
			_t("TopicPage.ItemsPerPage", "Įrašų per puslapį")
			), "Content"
		);

		return $fields;
	}

	public function TopicAmount($perPage = 1){
		$TopicAmount = singleton("TopicAmountObject")
			->get()
			->filter(array(
				"Locale" => Translatable::get_current_locale()
			))
			->sort("Created", "desc");

		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($TopicAmount->toArray()));
		$paginator->setCurrentPageNumber(
			(isset($_REQUEST["page"])) ? (int) $_REQUEST["page"] : 1
		);

		if( !empty($this->ItemsPerPage)){
			$perPage = $this->ItemsPerPage;
		}

		$paginator->setItemCountPerPage($perPage);

        $data = new ArrayList(
        	iterator_to_array($paginator->getCurrentItems(), false)
        	
        );

		$gettedPages = $paginator->getPages();

		if(is_array($gettedPages->pagesInRange)){
			$temp_pagesInRange = array();
			$pagesInRange = $gettedPages->pagesInRange;
			
			foreach($pagesInRange as $key => $value){
				array_push($temp_pagesInRange, array("item" => $value));
			}

			$gettedPages->pagesInRange = new ArrayList($temp_pagesInRange);
		}

		$data->pages = new ArrayData($gettedPages);

		$data->currentPage = (isset($_REQUEST["page"])) ? (int) $_REQUEST["page"] : 1;

		return $data;

	}

	public function getLinkOfMembersPage(){
		return singleton("MembersPage")->get()->filter(array("Locale" => Translatable::get_current_locale()))->First()->Link();
	}


	public function GetEditTopicTypePage(){
		return singleton("EditTopicTypepage")->get()
		       ->filter(array(
		       		"ParentID" => $this->ID,
		       		"Locale" => Translatable::get_current_locale()
		       	))
		       ->First();
	}

}

class TopicAmountPage_Controller extends Page_Controller {

	/**
	 * 
	 * defining to allowed controller url
	 * for some actions by methods. 
	 *
	 * @var array
	 * @static $allowed_actions
	 *
	*/
	private static $allowed_actions = array(
		"edit", "delete", 
		"TopicAmountForm", "add_new_amount"
	);

	/**
	 *
	 * initializing controller
	 *
	 * @param none
	 *
	*/
	public function init(){
		parent::init();

	}

	public function add_new_amount(){
		if($this->isSYSAdmin() || $this->isManager()){

			$data = array(
				"Title" => _t("TopicAmountPage.AddNewAmount", "Add new topic amount"),
				"Form" => new TopicAmountForm($this, "TopicAmountForm")
			);

			return $this->customise($data)
	        ->renderWith(array(
	       		"TopicAmountPage_add_new_amount", "Page"
	        ));
		}

		return $this->redirectBack();
	}

	public function edit($request){
		if($this->isSYSAdmin() || $this->isManager()){
			$data = array(
				"Title" => _t("TopicAmountPage.EditAmount", "Edit topic amount"),
				"Form" => new TopicAmountForm($this, "TopicAmountForm", $request->param("ID")),
				"data" => null
			);

			return $this->customise($data)
			->renderWith(array(
				"TopicAmountPage_add_new_amount", "Page"
			));
		}
	}

	public function delete($request){
		$data = singleton("TopicAmountObject")->get()->filter(array("ID" => $request->param("ID")));
		
		// delete object if find
		if($data->Count() == 1){
			
			$data->First()->delete();
		}

		// redirect back to page
		return $this->redirectBack();
	}

	public function TopicAmountForm(){
		$Form = new TopicAmountForm($this, "TopicAmountForm");
		$Form->clearMessage();

		return $Form;
	}

}