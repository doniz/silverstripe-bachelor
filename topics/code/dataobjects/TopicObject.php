<?php

class TopicObject extends DataObject {

	private static $db = array(
		"SortOrder"     => "Int",
		"Title"         => "Varchar",
		"Description"   => "HTMLText",
		"Locale"        => "Varchar",
		"Active"        => "Boolean(true)",
		"Status"        => "Enum(array('confirmed', 'waiting', 'unconfirmed', 'canceled'), 'unconfirmed')"
	);

	private static $defaults = array(
		"Status"        => "unconfirmed"
	);

	private static $has_one = array(
		"Lecturer"      => "Member",
		"Student"       => "Member",
		"Creator"       => "Member",
		"TopicAmount"   => "TopicAmountObject",
		"WorkFiles"     => "File"
	);

    private static $searchable_fields = array();
    
    private static $summary_fields = array(
        "Title",
        "DescriptionXML",
        "StatusXML"
    );

    public function CreatorURLSegment(){
        $Creator = $this->Creator();

        if($Creator){
            return singleton("SiteTree")->get()->First()->generateURLSegment(
                sprintf("%s-%s",
                    $Creator->Name,
                    $Creator->ID
                )
            );
        }

        return null;
    }

    public function LecturerURLSegment(){
        $Lecturer = $this->Lecturer();

        if($Lecturer){
            return singleton("SiteTree")->get()->First()->generateURLSegment(
                sprintf("%s-%s",
                    $Lecturer->Name,
                    $Lecturer->ID
                )
            );
        }

        return null;
    }

    public function StudentURLSegment(){
        $Student = $this->Student();

        if($Student){
            return singleton("SiteTree")->get()->First()->generateURLSegment(
                sprintf("%s-%s",
                    $Student->Name,
                    $Student->ID
                )
            );
        }

        return null;
    }

    /**
     * Define sumaryfields;
     * @return array $summaryFields
     */
    public function summaryFields() {
        $summaryFields = parent::summaryFields();
        $summaryFields = array_merge(
            $summaryFields, 
            array(
                'Title' => _t("TopicObject.Title", "Darbo pavadinimas"),
                'DescriptionXML' => _t("TopicObject.Description", "Darbo aprašymas"),
                'StatusXML' => _t("TopicObject.Status", "Temos (darbo) statusas")
            )
        );

        return $summaryFields;
    }

    public function DescriptionXML(){
    	return Convert::html2raw($this->Description);
    }

    public function canDeleteLecturer(){
        $Member = Member::currentUser();

        if(!$Member && !$this->isLecturer()) Controller::curr()->redirectBack();

        return (!$this->Student()->ID || $this->Status != 'confirmed');
    }

    public function StatusXML(){
    	// Enum(array('confirmed', 'unconfirmed', 'canceled')
    	switch($this->Status){
    		case "confirmed":
    			return _t("TopicObject.Confirmed", "Patvirtinta");
    		break;

    		case "unconfirmed":
    			return _t("TopicObject.Unconfirmed", "Nepatvirtinta");
    		break;

    		case "canceled":
    			return _t("TopicObject.Canceled", "Atmesta / atšaukta");
    		break;

            case "waiting":
                return _t("TopicObject.Waiting", "Laukia patvirtinimo");
            break;
    	}

    	return null;

    }

    /**
     * Define translatable searchable fields
     * @return array $searchableFields translatable
     */
    public function searchableFields(){
        $searchableFields = parent::searchableFields();

        $searchableFields['Title'] = array(
            'field'  => 'TextField',
            'filter' => 'PartialMatchFilter',
            'title'  => _t("TopicObject.Title", "Darbo pavadinimas")
        );

        $searchableFields['Description'] = array(
            'field'  => 'TextareaField',
            'filter' => 'PartialMatchFilter',
            'title'  => _t("TopicObject.Description", "Darbo aprašymas")
        );

        /**
         * Add the translatable dropdown if we can translate.
         */
        if(class_exists('Translatable')){
            $translatable = Translatable::get_existing_content_languages('Page');
            if(count($translatable) > 1){
                $searchableFields['Locale'] = array(
                    'title' => _t('TopicObject.LOCALE', 'Kalba'),
                    'filter' => 'ExactMatchFilter',
                    'field' => 'DropdownField',
                );
            }
        }

        return $searchableFields;
    }

	public function onBeforeWrite(){
        $Locale    = $this->Locale;
        $CreatorID = $this->CreatorID;

		if(empty($CreatorID)){
			$CurrentMember = Member::currentUser();

			$this->CreatorID = $CurrentMember->ID;
		}

        if(empty($Locale) && class_exists("Translatable")){
            $this->Locale = Translatable::get_current_locale();
        }

		parent::onBeforeWrite();
	}

	public function getCMSFields(){
		
		// Working files can upload
		$WorkFiles = UploadField::create(
			"WorkFiles",
			_t("TopicObject.WorkFiles", "Pridėkite darbo (atliktus) failus")
		);
		$WorkFiles->setRightTitle(
			_t("TopicObject.WorkFilesRightText", "failas gali būti .zip, .rar, .tar, .gz, .bz2, .7z failų formatas")
		);
		$WorkFiles->setFolderName("Uploads/StudentsWorks");
		$WorkFiles->setAllowedExtensions(
			array("zip", "rar", "tar", "gz", "bz2", "7z")
		);

		$fields = new FieldList(
			$Title = TextField::create(
				"Title",
				_t("TopicObject.Title", "Darbo pavadinimas")
			),
			$Description = HtmlEditorField::create(
				"Description",
				_t("TopicObject.Description", "Darbo aprašymas")
			)->setRows(15),
            $Lecturer = DropdownField::create(
                "LecturerID",
                _t("TopicObject.Lecturer", "Dėstytojas"),
                singleton("Page")->get()->First()->GetLecturers()->map("ID", "Name")
            )->setEmptyString(_t("TopicObject.SelectLecturerUser", "-- Pasirinkite dėstytoją --")),
            $WorkFiles
		);

		// show student field and topic amount field for administrators
		if(Permission::check('ADMIN')){
			$fields->push(
	            $Student = DropdownField::create(
	                "StudentID",
	                _t("TopicObject.Student", "Studentas"),
	                singleton("Page")->get()->First()->GetStudents()->map("ID", "Name")
	            )->setEmptyString(_t("TopicObject.SelectStudentUser", "-- Pasirinkite studentą --"))
			);
			$fields->push(
				$TopicAmount = DropdownField::create(
					"TopicAmountID",
					_t("TopicObject.TopicAmount", "Tematikos kategoriją (kiekis)"),
					singleton("TopicAmountObject")->get()->map("ID", "summaryForTopicOject")
				)->setEmptyString(_t("TopicObject.SelectTopicCat", "-- Pasirinkite tematikos kategoriją --"))
			);
			$fields->push(
				$Status = OptionsetField::create(
					"Status",
					_t("TopicObject.Status", "Temos (darbo) statusas"),
					array(
                        "confirmed" => _t("TopicObject.Confirmed", "Patvirtinta"),
						"waiting" => _t("TopicObject.Waiting", "Laukia patvirtinimo"),
						"unconfirmed" => _t("TopicObject.Unconfirmed", "Nepatvirtinta"),
						"canceled" => _t("TopicObject.Canceled", "Atmesta / atšaukta")
					)
				)
			);
		}

		if(!empty($this->Created)){
			$fields->insertBefore(
				LiteralField::create(
					"Created",
					'<div id="Created" class="field text">
						<label class="left" style="color: #919191;">Sukūrimo diena</label>
						<div class="middleColumn" style="padding-top: 7px;">
							'.$this->Created.'
						</div>
					</div>'
				),
				"Title"
			);
		}

		if(!empty($this->TopicAmountID)){
			$ExpireFrom = $this->TopicAmount()->TopicTypes()->ExpireFrom;
			$ExpireTo = $this->TopicAmount()->TopicTypes()->ExpireTo;

			$fields->insertBefore(
				LiteralField::create(
					"Expires",
					'<div id="Expires" class="field text">
						<label class="left" style="color: #919191;">Terminas</label>
						<div class="middleColumn" style="padding-top: 7px;">
							'.$ExpireFrom.' - '.$ExpireTo.'
						</div>
					</div>'
				),
				"Title"
			);
		}

		$fields->push(
			$Active = CheckboxField::create(
                "Active",
                _t("TopicAmountObject.Active", "Ar aktyvuoti įrašą?"),
                true
            )
        );

		return $fields;
	}

    function providePermissions(){
        return array(
            "MANAGE_OBJECT_TOPIC" => "Manage Object Topics",
        );
    }

    public function canCreate($member = null) {
        return Permission::check('MANAGE_OBJECT_CREATE');
    }
    
    public function canEdit($member = null) {
        return Permission::check('MANAGE_OBJECT_EDIT');
    }
    
    public function canDelete($member = null) {
        return Permission::check('MANAGE_OBJECT_DELETE');
    }

    public function canView($member = null) {
        return Permission::check('MANAGE_OBJECT_VIEW');
    }

    /**
     * Define singular name translatable
     * @return string Singular name
     */
    public function singular_name() {
    	return _t("TopicObject.SINGULARNAME", "Darbų priskyrimas studentams");
    }
    
    /*
     * Define plural name translatable
     * @return string Plural name
    */ 
    public function plural_name() {
        return _t("TopicObject.PLURALNAME", "Darbų priskyrimas studentams");
    }
}