<?php

class TopicAmountObject extends DataObject implements PermissionProvider {

	private static $db = array(
		"SortOrder"       => "Int",
		"Amount"          => "Int",
		"MemberAI"        => "Int", // Member Added Item (who added that item?)
        "LecturerID"      => "Int", // Store User ID
        "Locale"          => "Varchar",
        "Active"          => "Boolean(true)",

        // for partial search by filters
        "sql_summary_lecture_name" => "Varchar",
        "sql_summary_creator_name" => "Varchar",
        "sql_summary_topic_type"   => "Varchar"
	);

	private static $has_one = array(
		"TopicTypes" => "TopicTypes"
	);

	private static $has_many = array(
		// "TopicObject" => "TopicObject"
	);

    private static $searchable_fields = array();
    
    private static $summary_fields = array(
        "summaryLecturerName",
        "summaryCreatorName",
        "summaryTopicType",
        "Amount"
    );

    // this function checking if can delete manager.
    // that item, if there is items of relations of
    // this item. Becouse all of those records will be remove.
    public function canDeleteManager(){
        $Member = Member::currentUser();

        if(!$Member) Controller::curr()->redirectBack();

        $TopicObject = singleton("TopicObject")->get()->filter(array("TopicAmountID" => $this->ID));

        return $TopicObject->Count() <= 0;
    }


    // summary fields
    public function summaryLecturerName(){ return $this->Lecturer()->getName(); }
    public function summaryCreatorName(){ return $this->Creator()->getName(); }
    public function summaryTopicType(){ return $this->TopicTypes()->Name(); }
    public function summaryForTopicOject(){
        return sprintf(
            "%s - %s",
            $this->Lecturer()->getName(),
            $this->TopicTypes()->Name()
        );
    }

    /**
     * Define sumaryfields;
     * @return array $summaryFields
     */
    public function summaryFields() {
        $summaryFields = parent::summaryFields();
        $summaryFields = array_merge(
            $summaryFields, 
            array(
                'summaryLecturerName' => _t("TopicAmountObject.summaryLecturerName", "Priskyrtas vartotojas"),
                'summaryCreatorName' => _t("TopicAmountObject.summaryCreatorName", "Įrašą sukūrė"),
                'summaryTopicType' => _t("TopicAmountObject.summaryTopicType", "Tipas"),
                'Amount' => _t("TopicAmountObject.AmountForLecturerTopics", "Kiekis dėstytojo temoms")
            )
        );

        return $summaryFields;
    }

    /**
     * Define translatable searchable fields
     * @return array $searchableFields translatable
     */
    public function searchableFields(){
        $searchableFields = parent::searchableFields();

        $searchableFields['Amount'] = array(
            'field'  => 'NumericField',
            'filter' => 'ExactMatchFilter',
            'title'  => _t("TopicAmountObject.AmountForLecturerTopics", "Kiekis dėstytojo temoms")
        );

        $searchableFields['sql_summary_lecture_name'] = array(
            'field'  => 'TextField',
            'filter' => 'PartialMatchFilter',
            'title'  => _t("TopicAmountObject.summaryLecturerName", "Priskyrtas vartotojas")
        );

        $searchableFields['sql_summary_creator_name'] = array(
            'field'  => 'TextField',
            'filter' => 'PartialMatchFilter',
            'title'  => _t("TopicAmountObject.summaryCreatorName", "Įrašą sukūrė")
        );

        $searchableFields['sql_summary_topic_type'] = array(
            'field'  => 'DateField',
            'filter' => 'PartialMatchFilter',
            'title'  => _t("TopicAmountObject.summaryTopicType", "Tipas")
        );


        /**
         * Add the translatable dropdown if we can translate.
         */
        if(class_exists('Translatable')){
            $translatable = Translatable::get_existing_content_languages('Page');
            if(count($translatable) > 1){
                $searchableFields['Locale'] = array(
                    'title' => _t('TopicTypes.LOCALE', 'Kalba'),
                    'filter' => 'ExactMatchFilter',
                    'field' => 'DropdownField',
                );
            }
        }

        return $searchableFields;
    }

	public function onBeforeWrite(){

		$MemberAI = $this->MemberAI;
        $Locale   = $this->Locale;

		if(empty($MemberAI)){
			$CurrentMember = Member::currentUser();

			$this->MemberAI = $CurrentMember->ID;
		}

        if(empty($Locale) && class_exists("Translatable")){
            $this->Locale = Translatable::get_current_locale();
        }

        $this->sql_summary_lecture_name = $this->summaryLecturerName();
        $this->sql_summary_creator_name = $this->summaryCreatorName();
        $this->sql_summary_topic_type = $this->summaryTopicType();

		parent::onBeforeWrite();
	}

    public function CreatesAmountLength(){
        $data = singleton("TopicObject")
            ->get()
            ->filter(array(
                "TopicAmountID" => $this->ID
            ));

        return $data->Count();
    }

    public function Lecturer(){
        $LecturerID = $this->LecturerID;

        if(is_numeric($LecturerID) || is_int($LecturerID)){
            return singleton("Page")
                ->get()
                ->first()
                ->getUserById($LecturerID);
        }

        return null;
    }

    public function LecturerURLSegment(){
        $Lecturer = $this->Lecturer();

        if($Lecturer){
            return singleton("SiteTree")->get()->First()->generateURLSegment(
                sprintf("%s-%s",
                    $Lecturer->Name,
                    $Lecturer->ID
                )
            );
        }

        return null;
    }

    public function Creator(){
        $MemberAI = $this->MemberAI;

        if(is_numeric($MemberAI) || is_int($MemberAI)){
            return singleton("Page")
                ->get()
                ->first()
                ->getUserById($MemberAI);
        }

        return null;
    }

    public function CreatorURLSegment(){
        $Creator = $this->Creator();

        if($Creator){
            return singleton("SiteTree")->get()->First()->generateURLSegment(
                sprintf("%s-%s",
                    $Creator->Name,
                    $Creator->ID
                )
            );
        }

        return null;
    }

	public function getCMSFields(){
		return FieldList::create(
            $Amount = NumericField::create(
                "Amount",
                _t("TopicAmountObject.AmountForLecturerTopics", "Kiekis dėstytojo temoms")
            ),
            $Lecturer = DropdownField::create(
                "LecturerID",
                _t("TopicAmountObject.Lecturer", "Dėstytojas"),
                singleton("Page")->get()->First()->GetLecturers()->map("ID", "Name")
            )->setEmptyString(_t("TopicAmountObject.SelectLecturerUser", "-- Pasirinkite dėstytoją --")),
            $TopicTypes = DropdownField::create(
                "TopicTypesID",
                _t("TopicAmountObject.TopicTypes", "Baigiamųjų darbų tipas"),
                singleton("TopicTypes")->get("TopicTypes")->map("ID", "Name")
            )->setEmptyString(_t("TopicAmountObject.SelectTopicType", "-- Pasirinkite darbo tipą --")),
            $Active = CheckboxField::create(
                "Active",
                _t("TopicAmountObject.Active", "Ar aktyvuoti įrašą?"),
                true
            )
		);
	}

    function providePermissions(){
        return array(
            "MANAGE_AMOUNT_TOPIC" => "Manage Amount Topics",
        );
    }

    public function canCreate($member = null) {
        return Permission::check('MANAGE_AMOUNT_CREATE');
    }
    
    public function canEdit($member = null) {
        return Permission::check('MANAGE_AMOUNT_EDIT');
    }
    
    public function canDelete($member = null) {
        return Permission::check('MANAGE_AMOUNT_DELETE');
    }

    public function canView($member = null) {
        return Permission::check('MANAGE_AMOUNT_VIEW');
    }

    /**
     * Define singular name translatable
     * @return string Singular name
     */
    public function singular_name() {
    	return _t("TopicAmountObject.SINGULARNAME", "Tematikų priskyrimas");
    }
    
    /*
     * Define plural name translatable
     * @return string Plural name
    */ 
    public function plural_name() {
        return _t("TopicAmountObject.PLURALNAME", "Tematikų priskyrimas");
    }
}