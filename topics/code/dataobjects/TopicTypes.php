<?php

class TopicTypes extends DataObject implements PermissionProvider {

	private static $db = array(
		"DegreeTitle" => "Varchar",
		"ExpireFrom"  => "Date",
		"ExpireTo"    => "Date",
        "MemberAI"    => "Int", // Member Added Item (who added that item?)
		"Locale"      => "Varchar",
        "Active"      => "Boolean(true)"
	);

	private static $many_many = array(
		"CourseGroup" => "Group"
	);

    private static $defaults = array(
        "Active" => true
    );

    private static $searchable_fields = array();
    
    private static $summary_fields = array(
        'DegreeTitle',
        'ExpireFrom',
        'ExpireTo',
        'ParsedCourseGroup'
    );

    /**
     * Define sumaryfields;
     * @return array $summaryFields
     */
    public function summaryFields() {
        $summaryFields = parent::summaryFields();
        $summaryFields = array_merge(
            $summaryFields, 
            array(
                'DegreeTitle' => _t("TopicTypes.DegreeTitle", "Pavadinimas (baigiamojo darbo)"),
                'ExpireFrom' => _t("TopicTypes.ExpireFrom", "Galioja nuo (terminas)"),
                'ExpireTo' => _t("TopicTypes.ExpireTo", "Galioja iki (terminas)"),
                'ParsedCourseGroup' => _t('TopicTypes.COURSEGROUP', 'Kurso grupės')
            )
        );

        return $summaryFields;
    }

    function providePermissions(){
        return array(
            "MANAGE_TOPIC" => "Manage Topics",
        );
    }

    public function canCreate($member = null) {
        return Permission::check('TOPIC_CREATE');
    }
    
    public function canEdit($member = null) {
        return Permission::check('TOPIC_EDIT');
    }
    
    public function canDelete($member = null) {
        return Permission::check('TOPIC_DELETE');
    }

    public function canView($member = null) {
        return Permission::check('TOPIC_VIEW');
    }

    /**
     * Define singular name translatable
     * @return string Singular name
     */
    public function singular_name() {
    	return _t("TopicTypes.SINGULARNAME", "Baigiamųjų darbų tipai");
    }
    
    /*
     * Define plural name translatable
     * @return string Plural name
    */ 
    public function plural_name() {
        return _t("TopicTypes.PLURALNAME", "Baigiamųjų darbų tipai");
    }

    public function Creator(){
        $MemberAI = $this->MemberAI;

        if(is_numeric($MemberAI) || is_int($MemberAI)){
            return singleton("Page")
                ->get()
                ->first()
                ->getUserById($MemberAI);
        }

        return null;
    }

    public function onBeforeWrite(){

        $MemberAI = $this->MemberAI;

        if(empty($MemberAI) || $MemberAI == 0){
            $CurrentMember = Member::currentUser();

            $this->MemberAI = $CurrentMember->ID;
        }

        parent::onBeforeWrite();
    }

    public function DegreeTitleURLSegment(){
        $DegreeTitle = $this->DegreeTitle;

        if(!empty($DegreeTitle)){
            return singleton("SiteTree")->get()->First()->generateURLSegment(
                sprintf("%s-%s",
                    $DegreeTitle,
                    $this->ID
                )
            );
        }

        return false;
    }

    public function CreatorURLSegment(){
        $Creator = $this->Creator();

        if($Creator){
            return singleton("SiteTree")->get()->First()->generateURLSegment(
                sprintf("%s-%s",
                    $Creator->Name,
                    $Creator->ID
                )
            );
        }

        return null;
    }

	public function getCMSFields(){
		return FieldList::create(
			$DegreeTitle = TextField::create(
				"DegreeTitle",
				_t("TopicTypes.DegreeTitle", "Pavadinimas (baigiamojo darbo)")
			),
			$ExpireFrom = DateField::create(
				"ExpireFrom",
				_t("TopicTypes.ExpireFrom", "Galioja nuo (terminas)")
			)->setConfig("showcalendar", true),
			$ExpireTo = DateField::create(
				"ExpireTo",
				_t("TopicTypes.ExpireTo", "Galioja iki (terminas)")
			)->setConfig("showcalendar", true),
			$CourseGroup = TreeMultiselectField::create(
				'CourseGroup',
				_t('TopicTypes.COURSEGROUP', 'Kurso grupės'),
				'Group'
			),
            $Active = CheckboxField::create(
                "Active",
                _t("TopicTypes.Active", "Ar aktyvuoti įrašą?")
            ),
			$Locale = HiddenField::create(
				"Locale",
				null,
				Translatable::get_current_locale()
			)
		);

        $ExpireFrom->setConfig('dateformat', 'yyyy-MM-dd');
        $ExpireTo->setConfig('dateformat', 'yyyy-MM-dd');
	}

    public function Name(){
        return sprintf(
            "%s - %s",
            $this->DegreeTitle,
            $this->ParsedCourseGroup()
        );
    }

	public function ParsedCourseGroup(){
		$CourseGroup = $this->CourseGroup();
		$TempCourseGroup = array();

		foreach($CourseGroup as $Group){
			array_push($TempCourseGroup, $Group->Title);
		}

		return implode(", ", $TempCourseGroup);
	}


    /**
     * Setup the translatable dropdown sources.
     * @param array $_params
     * @return FieldList $fields
     */
    public function scaffoldSearchFields($_params = null){
        $fields = parent::scaffoldSearchFields($_params);

        if($fields->fieldByName('ExpireFrom') != null)
        {
            $fields->fieldByName('ExpireFrom')
                   ->setTitle(_t("TopicTypes.ExpireFrom", "Galioja nuo (terminas)"));
        }

        if($fields->fieldByName('ExpireTo') != null)
        {
            $fields->fieldByName('ExpireTo')
                   ->setTitle(_t("TopicTypes.ExpireTo", "Galioja iki (terminas)"));
        }

        if($fields->fieldByName('DegreeTitle') != null)
        {
            $fields->fieldByName('DegreeTitle')
                   ->setTitle(_t("TopicTypes.DegreeTitle", "Pavadinimas (baigiamojo darbo)"));
        }

        if($fields->fieldByName('ParsedCourseGroup') != null)
        {
            $fields->fieldByName('ParsedCourseGroup')
                   ->setTitle(_t('TopicTypes.COURSEGROUP', 'Kurso grupės'));
        }

        return $fields;
    }

	/**
	 * Define translatable searchable fields
	 * @return array $searchableFields translatable
	 */
	public function searchableFields(){
		$searchableFields = parent::searchableFields();
		// unset($searchableFields['NaujienosHolder.Title']);
		// unset($searchableFields['Publish']);

		$searchableFields['DegreeTitle'] = array(
			'field'  => 'TextField',
			'filter' => 'PartialMatchFilter',
			'title'  => _t("TopicTypes.DegreeTitle", "Pavadinimas (baigiamojo darbo)")
		);

		$searchableFields['ExpireFrom'] = array(
			'field'  => 'DateField',
			'filter' => 'ExactMatchFilter',
			'title'  => _t("TopicTypes.ExpireFrom", "Galioja nuo (terminas)")
		);

		$searchableFields['ExpireTo'] = array(
			'field'  => 'DateField',
			'filter' => 'ExactMatchFilter',
			'title'  => _t("TopicTypes.ExpireTo", "Galioja iki (terminas)")
		);


		/**
		 * Add the translatable dropdown if we can translate.
		 */
		if(class_exists('Translatable')){
			$translatable = Translatable::get_existing_content_languages('Page');
			if(count($translatable) > 1){
				$searchableFields['Locale'] = array(
					'title' => _t('TopicTypes.LOCALE', 'Kalba'),
					'filter' => 'ExactMatchFilter',
					'field' => 'DropdownField',
				);
			}
		}

		return $searchableFields;
	}


}