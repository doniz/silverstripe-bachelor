<?php

class TopicAdmin extends ModelAdmin {

    private static $managed_models = array(
        'TopicTypes', 'TopicAmountObject',
        'TopicObject'
    );

    private static $url_segment = 'topics';

    private static $menu_title = 'Topics';
    
    public $showImportForm = false;

    public function getEditForm($id = null, $fields = null) {
        $form = parent::getEditForm($id, $fields);
        $gridField = $form->Fields()->fieldByName($this->sanitiseClassName($this->modelClass));
        // $gridField->getConfig()->addComponent(new GridFieldSortableRows('SortOrder'));
        $siteConfig = SiteConfig::current_site_config();

        if($this->modelClass == "TopicTypes" && !$siteConfig->AllowExport){
            $form->Fields()
                ->fieldByName("TopicTypes")
                ->getConfig()
                ->removeComponentsByType('GridFieldExportButton')
                ->getComponentByType("GridFieldAddNewButton")
                ->setButtonName(
                    _t("TopicAdmin.NewRecord", "Naujas įrašas")
                );
        }

        if($this->modelClass == "TopicAmountObject" && !$siteConfig->AllowExport){
            $form->Fields()
                ->fieldByName("TopicAmountObject")
                ->getConfig()
                ->removeComponentsByType('GridFieldExportButton')
                ->getComponentByType("GridFieldAddNewButton")
                ->setButtonName(
                    _t("TopicAdmin.NewRecord", "Naujas įrašas")
                );
        }

        if($this->modelClass == "TopicObject" && !$siteConfig->AllowExport){
            $form->Fields()
                ->fieldByName("TopicObject")
                ->getConfig()
                ->removeComponentsByType('GridFieldExportButton')
                ->getComponentByType("GridFieldAddNewButton")
                ->setButtonName(
                    _t("TopicAdmin.NewRecord", "Naujas įrašas")
                );
        }

        return $form;
    }

}