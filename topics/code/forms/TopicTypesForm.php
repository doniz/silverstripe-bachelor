<?php

class TopicTypesForm extends Form {

    public $currentDataType = null;

    public $selected_data = null;

    public $dropdown_data = null;

    public function setCurrent($id = 0){
        if($id != 0 && is_int((int) $id)){
            $data = singleton("TopicTypes")->get()->filter(array("ID" => $id));

            if($data->Count() == 1){
                $this->currentDataType = $data->First();

                return true;
            }
        }

        return false;
    }

    public function __construct($controller, $name, $setCurrent = null){

        if(!is_null($setCurrent)){
            $this->setCurrent($setCurrent);
        }

        $temp_groups = array();
        foreach(singleton("TopicTypes")->get() as $Type){
            $temp_type_groups = $Type->CourseGroup()->map("ID")->toArray();
            // $temp_groups = array_merge($temp_type_groups, $temp_groups);
            foreach($temp_type_groups as $key => $value){
                $temp_groups[$key] = $value;
            }
        }


        $CourseGroup = singleton("Group")
            ->get("Group", "ParentID != 0")
            ->map("ID", "Title")
            ->toArray();

        // $CourseGroup = array_merge($CourseGroup, $temp_groups);

        if(!is_null($this->currentDataType)){
            foreach($this->currentDataType->CourseGroup()->map("ID", "Title")->toArray() as $key => $value){
                if(isset($temp_groups[$key]) && $temp_groups[$key] == $value){
                    unset($temp_groups[$key]);
                }
            }
        }

        foreach($CourseGroup as $key => $value){
            if(isset($temp_groups[$key]) && $temp_groups[$key] == $value){
                unset($CourseGroup[$key]);
            }
        }

        $temp_sel_data = array();
        if(!is_null($this->currentDataType)){
            $this->selected_data = new ArrayList();
            foreach($this->currentDataType->CourseGroup()->map("ID", "Title")->toArray() as $key => $value){
                $this->selected_data->push(new ArrayData(array(
                    "_key" => $key,
                    "_value" => $value
                )));

                $temp_sel_data[$key] = $value;
            }
            
        }


        $this->dropdown_data = new ArrayList();
        foreach($CourseGroup as $key => $value){
            $this->dropdown_data->push(new ArrayData(array(
                "option" => $key,
                "value"  => $value,
                "select" => (array_key_exists($key, $temp_sel_data) ? 1 : 0)
            )));
        }


        $fields = new FieldList(

            $DegreeTitle = TextField::create(
                "DegreeTitle",
                _t("TopicTypes.DegreeTitle", "Pavadinimas (baigiamojo darbo)"),
                (!is_null($this->currentDataType) ? $this->currentDataType->DegreeTitle : null)
            )->addExtraClass('form-control')
             ->setAttribute('placeholder', _t("TopicTypes.DegreeTitle", "Pavadinimas (baigiamojo darbo)")),

            $ExpireFrom = DateField::create(
                "ExpireFrom",
                _t("TopicTypes.ExpireFrom", "Galioja nuo (terminas)"),
                (!is_null($this->currentDataType) ? $this->currentDataType->ExpireFrom : null)
            )->addExtraClass('form-control datepicker')
             ->setAttribute('placeholder', _t("TopicTypes.ExpireFrom", "Galioja nuo (terminas)"))
             ->setAttribute('data-date-format', 'yyyy-mm-dd')
             ->setAttribute('data-mask', '9999-99-99'),

            $ExpireTo = DateField::create(
                "ExpireTo",
                _t("TopicTypes.ExpireTo", "Galioja iki (terminas)"),
                (!is_null($this->currentDataType) ? $this->currentDataType->ExpireTo : null)
            )->addExtraClass('form-control datepicker')
             ->setAttribute('placeholder', _t("TopicTypes.ExpireTo", "Galioja iki (terminas)"))
             ->setAttribute('data-date-format', 'yyyy-mm-dd')
             ->setAttribute('data-mask', '9999-99-99'),

            $CourseGroup = DropdownField::create(
                'CourseGroup[]',
                _t('TopicTypes.COURSEGROUP', 'Kurso grupės'),
                $CourseGroup
            )->addExtraClass('form-control select2 select2-offscreen')
             ->setAttribute('multiple', 'multiple')
             ->setAttribute('placeholder', _t('TopicTypes.COURSEGROUP', 'Kurso grupės')),

            $Active = CheckboxField::create(
                "Active",
                _t("TopicTypes.Active", "Ar aktyvuoti įrašą?"),
                (!is_null($this->currentDataType) && $this->currentDataType->Active ? true : false)
            )->addExtraClass('form-control')
            
        );

        if(!is_null($this->currentDataType)){
            $fields->push(
                new HiddenField(
                    "update",
                    "update",
                    1
                )
            );
        }

        if(!is_null($this->currentDataType)){
            $fields->push(new HiddenField("ID", "ID", $setCurrent));
        }

        $actions = new FieldList(
            FormAction::create("SubmitType")
                ->setTitle(_t('TopicTypesForm.Submit', "Submit"))
                ->addExtraClass('btn btn-primary')
        );

        $validator = new RequiredFields('DegreeTitle', 'ExpireFrom', 'ExpireFrom', 'ExpireTo');

        parent::__construct($controller, $name, $fields, $actions, $validator);
    }

    public function YOLO(){
        return $this->selected_data;
    }

    public function YOLO2(){
        return $this->dropdown_data;
    }
    
    public function Success() {
        $submitted = Session::get('TopicSubmitted');
        Session::clear('TopicSubmitted');
        return !empty($submitted);
    }

    public function isUpdated(){
        $data = Controller::curr()->request;
        $updated = Session::get('TopicUpdate');
        Session::clear('TopicUpdate');

        if($data->param("ID") && !empty($updated)){
            $object = singleton("TopicTypes")->get()->filter(array(
                "ID" => $data->param("ID")
            ));

            if($object->Count() > 0){
                return true;
            }
        }

        return false;
    }

    public function SubmitType(array $data, Form $form){
        $Member = Member::currentUser();

        if(!$Member) Controller::curr()->redirectBack();

        $TopicType = new TopicTypes();

        if($data["ID"]){
            $TopicType->ID = $data["ID"];
        }

        $TopicType->MemberAI = $Member->ID;
        $TopicType->Locale   = Translatable::get_current_locale();

        if(isset($data["update"]) && $data["update"]){
            foreach($TopicType->CourseGroup() as $TopicGroup){
                $TopicType->CourseGroup()->remove($TopicGroup);
            }
        }

        $TopicType->CourseGroup()->addMany(
            singleton("Group")
                ->get()
                ->filter(array(
                    "ID" => $data["CourseGroup"]
                ))
        );

        $form->saveInto($TopicType);

        $TopicType->write();

        if(isset($data["update"]) && $data["update"]){
            Session::set('TopicUpdate', true);
        } else {
            Session::set('TopicSubmitted', true);
        }

        return Controller::curr()->redirectBack();
    }

    public function TopicPageLink(){
        return singleton("TopicPage")->get()->filter(array("Locale" => Translatable::get_current_locale()))->First()->Link();
    }

    public function forTemplate() {
        return $this->renderWith(array($this->class, 'Form'));
    }

}