<?php

class TopicAmountForm extends Form {

    public $currentDataType = null;

    public $selected_data = null;

    public $dropdown_data = null;

    public function setCurrent($id = 0){
        if($id != 0 && is_int((int) $id)){
            $data = singleton("TopicAmountObject")->get()->filter(array("ID" => $id));

            if($data->Count() == 1){
                $this->currentDataType = $data->First();

                return true;
            }
        }

        return false;
    }

    public function __construct($controller, $name, $setCurrent = null){

        if(!is_null($setCurrent)){
            $this->setCurrent($setCurrent);
        }

        $lecturers = ($this->currentDataType) ?
            singleton("Page")
            ->get()
            ->First()
            ->GetLecturers()
            ->filter(array(
                "ID:not" => 
                singleton("TopicAmountObject")->get()->exclude('LecturerID', $this->currentDataType->LecturerID)->map("LecturerID", "LecturerID")->toArray()
            ))
            ->map("ID", "Name")
            :
            singleton("Page")
            ->get()
            ->First()
            ->GetLecturers()
            ->filter(array(
                "ID:not" => 
                singleton("TopicAmountObject")->get()->map("LecturerID", "LecturerID")->toArray()
            ))
            ->map("ID", "Name");


        $fields = new FieldList(


            $Amount = NumericField::create(
                "Amount",
                _t("TopicAmountObject.AmountForLecturerTopics", "Kiekis dėstytojo temoms"),
                (!is_null($this->currentDataType) ? $this->currentDataType->Amount : null)
            )->addExtraClass('form-control')
             ->setAttribute('placeholder', _t("TopicAmountObject.AmountForLecturerTopics", "Kiekis dėstytojo temoms")),

            $Lecturer = DropdownField::create(
                "LecturerID",
                _t("TopicAmountObject.Lecturer", "Dėstytojas"),

                $lecturers,
                (!is_null($this->currentDataType) ? $this->currentDataType->LecturerID : null)

             )->setEmptyString(_t("TopicAmountObject.SelectLecturerUser", "-- Pasirinkite dėstytoją --"))
              ->addExtraClass('form-control select2 select2-offscreen')
              ->setAttribute('placeholder', _t("TopicAmountObject.Lecturer", "Dėstytojas")),

            $TopicTypes = DropdownField::create(
                "TopicTypesID",
                _t("TopicAmountObject.TopicTypes", "Baigiamųjų darbų tipas"),
                singleton("TopicTypes")->get("TopicTypes")->map("ID", "Name"),
                (!is_null($this->currentDataType) ? $this->currentDataType->TopicTypesID : null)
            )->setEmptyString(_t("TopicAmountObject.SelectTopicType", "-- Pasirinkite darbo tipą --"))
             ->addExtraClass('form-control select2 select2-offscreen')
             ->setAttribute('placeholder', _t("TopicAmountObject.TopicTypes", "Baigiamųjų darbų tipas")),

            $Active = CheckboxField::create(
                "Active",
                _t("TopicAmountObject.Active", "Ar aktyvuoti įrašą?"),
                (!is_null($this->currentDataType) && $this->currentDataType->Active ? true : false)
            )->addExtraClass('form-control')
            
        );

        if(!is_null($this->currentDataType)){
            $fields->push(new HiddenField("ID", "ID", $setCurrent));
            $fields->push(
                new HiddenField(
                    "update",
                    "update",
                    1
                )
            );
        }

        $actions = new FieldList(
            FormAction::create("SubmitAmount")
                ->setTitle(_t('TopicAmountForm.Submit', "Submit"))
                ->addExtraClass('btn btn-primary')
        );

        $validator = new RequiredFields('Amount', 'LecturerID', 'TopicTypesID');

        parent::__construct($controller, $name, $fields, $actions, $validator);
    }
    
    public function Success() {
        $submitted = Session::get('TopicSubmitted');
        Session::clear('TopicSubmitted');
        return !empty($submitted);
    }

    public function TopicTypesData(){
        if(!is_null($this->currentDataType)){
            return singleton("TopicTypes")
                    ->get()
                    ->filter(array(
                        "ID" => singleton("TopicAmountObject")
                            ->get()
                            ->filter(array(
                                "ID" => $this->currentDataType->ID
                            ))
                            ->First()
                            ->TopicTypesID
                    ))
                    ->First();
        }
    }

    public function isUpdated(){
        $data = Controller::curr()->request;
        $updated = Session::get('TopicUpdate');
        Session::clear('TopicUpdate');

        if($data->param("ID") && !empty($updated)){
            $object = singleton("TopicAmountObject")->get()->filter(array(
                "ID" => $data->param("ID")
            ));

            if($object->Count() > 0){
                return true;
            }
        }

        return false;
    }

    public function SubmitAmount(array $data, Form $form){
        $Member = Member::currentUser();

        if(!$Member) Controller::curr()->redirectBack();

        $TopicAmount = new TopicAmountObject();

        if(isset($data["ID"])){
            $TopicAmount->ID = $data["ID"];
        }


        $TopicAmount->MemberAI = $Member->ID;
        $TopicAmount->Locale   = Translatable::get_current_locale();
        $TopicAmount->LecturerID = $data["LecturerID"];
        $TopicAmount->TopicTypesID = $data["TopicTypesID"];

        $form->saveInto($TopicAmount);

        $TopicAmount->write();

        if(isset($data["update"]) && $data["update"]){
            Session::set('TopicUpdate', true);
        } else {
            Session::set('TopicSubmitted', true);
        }

        return Controller::curr()->redirectBack();
    }

    public function TopicAmountPageLink(){
        return singleton("TopicAmountPage")->get()->filter(array("Locale" => Translatable::get_current_locale()))->First()->Link();
    }

    public function forTemplate() {
        return $this->renderWith(array($this->class, 'Form'));
    }

}