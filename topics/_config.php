<?php

/**
 *
 * Define constant of path to module
 *
 * @var string/const
 * TOPICS_PATH
 *
*/
define('TOPICS_PATH', basename(__DIR__));

set_include_path(
	get_include_path() . PATH_SEPARATOR . BASE_PATH . '/thirdparty/'
);

require_once BASE_PATH . "/thirdparty/Zend/Paginator.php";
require_once BASE_PATH . "/thirdparty/Zend/Paginator/Adapter/Array.php";