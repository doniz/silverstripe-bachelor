<?php

class TranslationCollector extends Object implements i18nEntityProvider {
	
	protected static $_collectableModules = array();
	
	// use this in _config.php
	// ex. TranslationCollector::setCollectableModules(array('faq', 'news', 'themes/mytheme'));
	public static function setCollectableModules($collectableModules) {
		self::$_collectableModules = $collectableModules;
	}
	
	protected function _getLangFile($module, $language, $locale) {
		$langFile = Director::baseFolder() . '/' . $module . '/lang/' . $language . '.yml';
		if (!is_file($langFile)) {
			$language = $locale;
			$langFile = Director::baseFolder() . '/' . $module . '/lang/' . $language . '.yml';
		}
		return array($langFile, $language);
	}
	
	public function provideI18nEntities() {
		require_once 'thirdparty/zend_translate_railsyaml/library/Translate/Adapter/thirdparty/sfYaml/lib'
			. '/sfYamlParser.php';
//		$entities = parent::provideI18nEntities();
		$entities = array();
		if (empty(self::$_collectableModules)) {
			return $entities;
		}
//		if (get_class($this) != 'TranslationCollector') {
//			return $entities;
//		}
		
		$currentLocale = Translatable::get_current_locale();
		$currentLanguage = i18n::get_lang_from_locale($currentLocale);
		
		list($langFile, $currentLocale) = $this->_getLangFile('langed', $currentLanguage, $currentLocale);
		
		$sfYaml = new sfYaml();
		
		$langed = $sfYaml->load($langFile);
		
		// file does not exist -> $mysite === $langFile 
		if ($langed === $langFile) {
			$langed = array();
		}
		
		$returnMyself = (Controller::curr()->getRequest()->getVar('module') != 'langed');
		if ($returnMyself && !empty($langed[$currentLocale])) {
			foreach ($langed[$currentLocale] as $key => $translations) {
				if (empty($translations)) {
					continue;
				}
				foreach ($translations as $subKey => $translation) {
					$entities[$key . '.' . $subKey] = array($translation);
				}
			}
			return $entities;
		}
		
		foreach (self::$_collectableModules as $module) {
			list($langFile, $currentLocale) = $this->_getLangFile($module, $currentLanguage, $currentLocale);
			if (is_file($langFile)) {
				$parsed = $sfYaml->load($langFile);
				if (empty($parsed[$currentLocale])) {
					continue;
				}
				foreach ($parsed[$currentLocale] as $key => $translations) {
					if (empty($translations)) {
						continue;
					}
					foreach ($translations as $subKey => $translation) {
						// Leave existing translation intact
						$entities[$key . '.' . $subKey] = 
							empty($langed[$currentLocale][$key][$subKey])
								? array($translation)
								: array($langed[$currentLocale][$key][$subKey]);
					}
				}
				// we want langed/lang to be the only translation source
				// ex. themes/mycooltheme overtakes langed/lang files even with i18n.yml file priorities
				@unlink($langFile);
			}
		}
		return $entities;
	}
	
}