<?php

class Langed extends LeftAndMain implements PermissionProvider {
	private static $url_segment = 'langed';
	private static $menu_title = 'Language Editor';
	
	public function init() {
		// HACKFIX when locale param is equal to this: 'en_US?locale=en_US'
		$locale = $this->getRequest()->getVar('locale');
		$locale = explode('?', $locale);
		Translatable::set_current_locale($locale[0]);
		
		parent::init();
		Requirements::javascript("langed/javascript/Langed.js");
		Requirements::css("langed/css/Langed.css");
	}


	public function providePermissions() {
		$title = _t("Langed.MENUTITLE", LeftAndMain::menu_title_for_class($this->class));
		return array(
			"CMS_ACCESS_Langed" => array(
				'name' => _t('CMSMain.ACCESS', "Access to '{title}' section", array('title' => $title)),
				'category' => _t('Permission.CMS_ACCESS_CATEGORY', 'CMS Access')
			)
		);
	}
	
	public function Breadcrumbs($unlinked = false) {
		$defaultTitle = self::menu_title_for_class(get_class($this));
		return new ArrayList(array(
			new ArrayData(array(
				'Title' => _t("{$this->class}.MENUTITLE", $defaultTitle),
				'Link' => false
			))
		));
	}
	
	public function getResponseNegotiator() {
		$neg = parent::getResponseNegotiator();
		$controller = $this;
		$neg->setCallback('CurrentForm', function() use(&$controller) {
			return $controller->renderWith($controller->getTemplatesWithSuffix('_Content'));
		});
		return $neg;
	}
	
	public function getEditForm($id = null, $fields = null) {
		require_once 'thirdparty/zend_translate_railsyaml/library/Translate/Adapter/thirdparty/sfYaml/lib'
			. '/sfYamlParser.php';
		
		$fields = new FieldList();
		
		$currentLocale = Translatable::get_current_locale();
		
		$currentLanguage = i18n::get_lang_from_locale($currentLocale);
		
//		$languages = Translatable::get_allowed_locales();
//		$languages = array_combine($languages, $languages);
//		$fields->push(new DropdownField('language', _t("{$this->class}.LANGUAGE_TITLE", 'Language'), $languages));
		$localeField = new LanguageDropdownField(
			'Locale', 
			_t('CMSMain.LANGUAGEDROPDOWNLABEL', 'Language'), 
			array(), 
			'SiteTree', 
			'Locale-English',
			singleton('SiteTree')
		);
		$localeField->setValue($currentLocale);
		$fields->push($localeField);
		
		$langFile = Director::baseFolder() . '/langed/lang/' . $currentLanguage . '.yml';
		if (!is_file($langFile)) {
			$currentLanguage = $currentLocale;
			$langFile = Director::baseFolder() . '/langed/lang/' . $currentLanguage . '.yml';
		}
		
		if (is_file($langFile)) {
			$sfYaml = new sfYaml();
//			$mysite = \Symfony\Component\Yaml\Yaml::parse($langFile);
			$mysite = $sfYaml->load($langFile);
			foreach ($mysite[$currentLanguage] as $key => $subkeys) {
				foreach ($subkeys as $subkey => $value) {
					$translation = new TextField('values[' . ($key . '.' . $subkey) . ']', $key . '.' . $subkey);
					$translation->setValue($value);
					$fields->push($translation);
				}
			}
			$langField = new HiddenField('Language');
			$langField->setValue($currentLanguage);
			$fields->push($langField);
		}
		
		$actions = new FieldList(
			FormAction::create('doSubmit', _t('Langed.SAVE','Save'))
				->addExtraClass('ss-ui-action-constructive')
				->setAttribute('data-icon', 'accept')
		);
		
		$form = new Form($this, 'EditForm', $fields, $actions);
		
		$form->addExtraClass('root-form');
		$form->addExtraClass('cms-edit-form cms-panel-padded center');
//		$form->addExtraClass('cms-edit-form');
		
		$form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
		
		return $form;
	}
	
	public function doSubmit(array $data, Form $form) {
		require_once 'thirdparty/zend_translate_railsyaml/library/Translate/Adapter/thirdparty/sfYaml/lib'
			. '/sfYamlParser.php';

		if (!empty($data['values'])) {
			$translations = array();
			foreach ($data['values'] as $key => $value) {
				$xKey = explode('.', $key);
				$translations[$xKey[0]][$xKey[1]] = $value;
			}
			$sfYaml = new sfYaml();
			file_put_contents(
				Director::baseFolder() . '/langed/lang/' . $data['Language'] . '.yml', 
//				\Symfony\Component\Yaml\Yaml::dump(array($data['Language'] => $translations), 99, 2)
				$sfYaml->dump(array($data['Language'] => $translations), 99)
			);
		}
		
		$this->response->addHeader('X-Status', rawurlencode(_t('LeftAndMain.SAVEDUP', 'Saved.')));

		// #2013-11-19, Fix: clear cache when is submitting new record.
		if( Director::isLive() || Director::isTest() ){
			// remove cache file from folder
			$cacheFolder = TEMP_FOLDER . "/cache/";

			// if existing cache folder
			if(is_dir($cacheFolder)){
				$files = glob($cacheFolder . "*");
				if(!empty($files)){
					foreach($files as $file){
						unlink($file);
					}
				}
			}

			SSViewer::flush_template_cache();
			
		}

		return $this->getResponseNegotiator()->respond($this->request);
	}
}